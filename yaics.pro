# -*- mode: makefile -*-
######################################################################
#  Copyright 2010-2015 Mats Sjöberg
#  Copyright 2014-2015 Stig Atle Steffensen
#  
#  This file is part of the Yaics application.
#
#  Yaics is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Yaics is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
######################################################################

QT += gui network xml webkit
lessThan(QT_MAJOR_VERSION, 5) {
    DEFINES += IS_QT4
} else {
    QT += widgets webkitwidgets
}

TARGET = yaics
TEMPLATE = app
INCLUDEPATH += src
OBJECTS_DIR = obj

# To enable debug mode, run as:
# qmake CONFIG+=debug
# or uncomment this:
# CONFIG+=debug

# Enable to generate etags automatically on make
#CONFIG += etags

unix:!macx {
    message("Enabling D-Bus")
    QT += dbus
    DEFINES += USE_DBUS
}

######################################################################
# Sources 
######################################################################

HEADERS      += src/desktop/yaicsapp.h \
                src/desktop/messagewidget.h \
                src/desktop/avatar.h \
                src/desktop/timeline.h \
                src/desktop/yaicstabwidget.h \
                src/desktop/messagewindow.h \
                src/desktop/statuswindow.h \
                src/desktop/directmessagewindow.h \
                src/desktop/messageedit.h \
                src/desktop/yaicshighlighter.h \
                src/desktop/statuslabel.h \
                src/desktop/yaicssettingsdialog.h \
                \
                src/util/util.h \
                src/util/yaicssettings.h \
                src/util/filedownloader.h \
                src/util/shorturlexpander.h \
                src/util/urlshortener.h \
                src/util/qaspell.h \
                src/util/yaics_defines.h \
                \
                src/qsocialapi/qsocialapi.h \
                src/qsocialapi/qsocialapirequest.h \
                src/qsocialapi/status.h \
                src/qsocialapi/messagelist.h \
                src/qsocialapi/user.h \
                src/qsocialapi/directmessage.h \
                src/qsocialapi/message.h

SOURCES      += src/desktop/main.cpp \
                src/desktop/yaicsapp.cpp \
                src/desktop/messagewidget.cpp \
                src/desktop/avatar.cpp \
                src/desktop/timeline.cpp \
                src/desktop/yaicstabwidget.cpp \
                src/desktop/messagewindow.cpp \
                src/desktop/statuswindow.cpp \
                src/desktop/directmessagewindow.cpp \
                src/desktop/messageedit.cpp \
                src/desktop/yaicshighlighter.cpp \
                src/desktop/statuslabel.cpp \
                src/desktop/yaicssettingsdialog.cpp \
                \
                src/util/yaicssettings.cpp \
                src/util/util.cpp \
                src/util/qaspell.cpp \
                src/util/filedownloader.cpp \
                src/util/shorturlexpander.cpp \
                src/util/urlshortener.cpp \
                \
                src/qsocialapi/qsocialapi.cpp \
                src/qsocialapi/qsocialapirequest.cpp \
                src/qsocialapi/status.cpp \
                src/qsocialapi/messagelist.cpp \
                src/qsocialapi/user.cpp \
                src/qsocialapi/directmessage.cpp \
                src/qsocialapi/message.cpp

FORMS        += src/ui/prefs.ui

TRANSLATIONS += translations/yaics_en.ts \
                translations/yaics_es.ts \
                translations/yaics_ru.ts \
                translations/yaics_uk.ts

RESOURCES    += res/yaics.qrc

win32:RC_FILE = res/yaics-win32.rc

# Optional spell checking support with libaspell.
exists( /usr/include/aspell.h )|exists( /usr/local/include/aspell.h ) {
    message("Using aspell")
    LIBS += -laspell
    DEFINES += USE_ASPELL
} else {
    warning("Unable to find libaspell header files for compiling. Install libaspell-dev (Debian, Ubuntu) if you want to enable spell-checking.")
}

# Optionally use etags
exists( /usr/bin/etags ) {
    message("Using etags")
    PRE_TARGETDEPS += etags
}

######################################################################
# Install target
######################################################################

unix {
    isEmpty(PREFIX) {
        PREFIX = /usr/local
    }
    BINDIR = $$PREFIX/bin
    DATADIR = $$PREFIX/share

    INSTALLS += target desktop icon16 icon32 icon48 icon64 appdata

    target.path = $$BINDIR

    desktop.path = $$DATADIR/applications
    desktop.files = $${TARGET}.desktop

    icon16.path = $$DATADIR/icons/hicolor/16x16/apps
    icon16.files = icons/16x16/$${TARGET}.png

    icon32.path = $$DATADIR/icons/hicolor/32x32/apps
    icon32.files = icons/32x32/$${TARGET}.png

    icon48.path = $$DATADIR/icons/hicolor/48x48/apps
    icon48.files = icons/48x48/$${TARGET}.png

    icon64.path = $$DATADIR/icons/hicolor/64x64/apps
    icon64.files = icons/64x64/$${TARGET}.png

    appdata.path = $$DATADIR/appdata
    appdata.files = $${TARGET}.appdata.xml
}

######################################################################
# Generate documentation
######################################################################

markdown.target   = %.html
markdown.commands = markdown $< > $@
markdown.depends  = %.md

DOC_TARGETS        = README.html
doc.depends        = $$DOC_TARGETS

######################################################################
# Generate TAGS for GNU Emacs and others
######################################################################

ETAGS_INPUTS   = $$HEADERS $$SOURCES
etags.depends  = $$ETAGS_INPUTS
etags.commands = etags -o src/TAGS $$ETAGS_INPUTS

######################################################################
# Add additional targets
######################################################################

QMAKE_EXTRA_TARGETS += etags markdown doc

######################################################################
# Extra stuff to clean up
######################################################################

QMAKE_CLEAN += $DOC_TARGETS src/TAGS

DISTFILES += \
    ChangeLog
