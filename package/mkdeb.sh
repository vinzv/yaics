#!/bin/bash
VERSION=$1
if [ -z "$VERSION" ]; then
    echo "Usage: $0 VERSION"
    echo "e.g. $0 0.4"
    exit 1
fi

OLD_FILES=$(find . -name "yaics*$VERSION*")

if [ ! -z "$OLD_FILES" ]; then
    echo "Old files for that version exist, please clean them up first:"
    echo $OLD_FILES
    exit 2
fi

DIR="yaics-${VERSION}"

# use this for real
#git clone git@gitlab.com:stigatle/yaics.git

# use this for testing
git clone .. yaics

mv yaics ${DIR}
rm -rf ${DIR}/.git*
tar czf yaics_${VERSION}.orig.tar.gz ${DIR}/
cd ${DIR}
dpkg-buildpackage -b
cd ..

echo "Running lintian ..."
lintian -i -I --show-overrides yaics_${VERSION}-1_*.changes

echo 
echo "Read the output above carefully!"
# echo "If all is OK you can run something like if you want to upload to Debian:"
# echo "dput mentors yaics_${VERSION}-1_i386.changes"
