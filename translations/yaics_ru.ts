<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>DirectMessageWindow</name>
    <message>
        <source>Write a direct message to </source>
        <translation>Написать личное сообщение </translation>
    </message>
    <message>
        <source>Send direct message</source>
        <translation>Отправить личное сообщение</translation>
    </message>
</context>
<context>
    <name>MessageWidget</name>
    <message>
        <source>reply</source>
        <translation>ответить</translation>
    </message>
    <message>
        <source>direct</source>
        <translation>ЛС</translation>
    </message>
    <message>
        <source>conversation</source>
        <translation>дискуссия</translation>
    </message>
    <message>
        <source> from </source>
        <translation> от </translation>
    </message>
    <message>
        <source>in context</source>
        <translation>в контексте</translation>
    </message>
    <message>
        <source>in reply to </source>
        <translation>в ответ на </translation>
    </message>
    <message>
        <source>unknown</source>
        <translation>неизвестный</translation>
    </message>
    <message>
        <source>[repeated by %1]</source>
        <translation>[повторил %1]</translation>
    </message>
    <message>
        <source>Mark notice as favourite</source>
        <translation>Отметить заметку как избранное</translation>
    </message>
    <message>
        <source>Repeat notice</source>
        <translation>Повторить заметку</translation>
    </message>
    <message>
        <source>Reply to notice</source>
        <translation>Ответить на заметку</translation>
    </message>
    <message>
        <source>Send direct message</source>
        <translation>Отправить личное сообщение</translation>
    </message>
    <message>
        <source>Open conversation tab</source>
        <translation>Открыть вкладку с дискуссией</translation>
    </message>
</context>
<context>
    <name>MessageWindow</name>
    <message>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <source>Send message</source>
        <translation>Отправить сообщение</translation>
    </message>
    <message>
        <source>Sending ...</source>
        <translation>Отправка ...</translation>
    </message>
</context>
<context>
    <name>Prefs</name>
    <message>
        <source>Preferences</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>&amp;Main</source>
        <translation>&amp;Основные</translation>
    </message>
    <message>
        <source>Update interval (in minutes):  </source>
        <translation>Интервал обновлений (в минутах):  </translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Уведомления</translation>
    </message>
    <message>
        <source>Home:</source>
        <translation>Главная:</translation>
    </message>
    <message>
        <source>Mentions:</source>
        <translation>Ответы:</translation>
    </message>
    <message>
        <source>Do nothing</source>
        <translation>Ничего не делать</translation>
    </message>
    <message>
        <source>Highlight tray icon</source>
        <translation>Подсветить значок в трее</translation>
    </message>
    <message>
        <source>Popup notification</source>
        <translation>Всплывающее уведомление</translation>
    </message>
    <message>
        <source>Filtering</source>
        <translation>Фильтрация</translation>
    </message>
    <message>
        <source>Text to filter:</source>
        <translation>Текст для фильтрации:</translation>
    </message>
    <message>
        <source>Enclose in /../ for regular expression.</source>
        <translation>Заключите в /../ для регулярного выражения.</translation>
    </message>
    <message>
        <source>Prepend replies with @username.</source>
        <translation>Начинать ответы с @username.</translation>
    </message>
    <message>
        <source>Show icon in system tray.</source>
        <translation>Показывать значок в трее.</translation>
    </message>
    <message>
        <source>Select theme:</source>
        <translation>Выбрать тему:</translation>
    </message>
    <message>
        <source>Set link colour</source>
        <translation>Указать цвет ссылок</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
    <message>
        <source>Enable &apos;Public&apos; tab.</source>
        <translation>Отображать вкладку &apos;Общее&apos;.</translation>
    </message>
    <message>
        <source>A&amp;ccount</source>
        <translation>У&amp;чётная запись</translation>
    </message>
    <message>
        <source>Username:</source>
        <translation>Имя пользователя:</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <source>Ignore SSL warnings</source>
        <translation>Игнорировать предупреждения SSL</translation>
    </message>
    <message>
        <source>P&amp;roxy</source>
        <translation>П&amp;рокси</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Отсутствует</translation>
    </message>
    <message>
        <source>Proxy type:</source>
        <translation>Тип прокси:</translation>
    </message>
    <message>
        <source>IP:</source>
        <translation>IP:</translation>
    </message>
    <message>
        <source>Port (TOR port is 9150):</source>
        <translation>Порт (порт TOR – 9150):</translation>
    </message>
    <message>
        <source>Socks5</source>
        <translation></translation>
    </message>
    <message>
        <source>HTTP</source>
        <translation></translation>
    </message>
    <message>
        <source>GNU social Instance:</source>
        <translation>Инстанс GNU social:</translation>
    </message>
</context>
<context>
    <name>StatusWindow</name>
    <message>
        <source>Write a reply to %1</source>
        <translation>Написать ответ %1</translation>
    </message>
    <message>
        <source>Send notice</source>
        <translation>Отправить заметку</translation>
    </message>
    <message>
        <source>Write a new notice</source>
        <translation>Написать новую заметку</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <source>Updated</source>
        <translation>Обновлено</translation>
    </message>
    <message>
        <source>Loading ...</source>
        <translation>Загрузка ...</translation>
    </message>
    <message>
        <source>Load older </source>
        <translation>Загрузить более старые </translation>
    </message>
    <message>
        <source>message</source>
        <translation>сообщение</translation>
    </message>
    <message>
        <source>update</source>
        <translation>обновление</translation>
    </message>
    <message>
        <source>messages</source>
        <translation>сообщения</translation>
    </message>
    <message>
        <source>updates</source>
        <translation>обновления</translation>
    </message>
</context>
<context>
    <name>YaicsApp</name>
    <message>
        <source>E&amp;xit</source>
        <translation>В&amp;ыход</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>&amp;Reload timeline</source>
        <translation>&amp;Перезагрузить ленту</translation>
    </message>
    <message>
        <source>Load &amp;older in timeline</source>
        <translation>Загрузить &amp;предыдущие записи</translation>
    </message>
    <message>
        <source>&amp;Pause home timeline</source>
        <translation>&amp;Приостановить ленту</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;О программе</translation>
    </message>
    <message>
        <source>New &amp;direct message</source>
        <translation>Новое &amp;личное сообщение</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Помощь</translation>
    </message>
    <message>
        <source>&lt;p&gt;Yaics is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;Yaics is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.&lt;/p&gt;&lt;p&gt;You should have received a copy of the GNU General Public License along with Yaics.  If not, see &lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;http://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Yaics – свободное программное обеспечение: вы можете распространять и/или изменять его согласно GNU General Public License, опубликованным Free Software Foundation, версии 3, либо (по вашему желанию) любой более поздней версии.&lt;/p&gt;&lt;p&gt;Yaics распространяется в надежде, что она будет полезной, но БЕЗ ВСЯКИХ ГАРАНТИЙ; в том числе неявных гарантий ТОВАРНОГО ВИДА и ГОДНОСТИ ДЛЯ ОПРЕДЕЛЁННОГО ПРИМЕНЕНИЯ.   Подробности смотрите в GNU General Public License.&lt;/p&gt;&lt;p&gt;Вы должны были получить копию GNU General Public License вместе с Yaics.  Если это не так, смотрите &lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;http://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Copyright &amp;copy; 2010-2015 Mats Sj&amp;ouml;berg</source>
        <translation></translation>
    </message>
    <message>
        <source>Copyright &amp;copy; 2014-2015 Stig Atle Steffensen</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;p&gt;Report bugs and feature requests at </source>
        <translation>&lt;p&gt;Уведомляйте об ошибках и пожеланиях в</translation>
    </message>
    <message>
        <source>A simple Qt-based GNU social client.</source>
        <translation>Простой клиент GNU social на Qt.</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation>О %1</translation>
    </message>
    <message>
        <source>&amp;Public</source>
        <translation>&amp;Общее</translation>
    </message>
    <message>
        <source>&amp;Home</source>
        <translation>&amp;Главная</translation>
    </message>
    <message>
        <source>&amp;Mentions</source>
        <translation>&amp;Упоминания</translation>
    </message>
    <message>
        <source>P&amp;rofile</source>
        <translation>П&amp;рофиль</translation>
    </message>
    <message>
        <source>&amp;Inbox</source>
        <translation>&amp;Входящие</translation>
    </message>
    <message>
        <source>&amp;Outbox</source>
        <translation>&amp;Исходящие</translation>
    </message>
    <message>
        <source>&amp;New notice</source>
        <translation>&amp;Новая заметка</translation>
    </message>
    <message>
        <source>Conversation</source>
        <translation>Дискуссия</translation>
    </message>
    <message>
        <source>&amp;Bugtracker</source>
        <translation>&amp;Багтрекер</translation>
    </message>
    <message>
        <source>Timelines are paused.</source>
        <translation>Ленты приостановлены.</translation>
    </message>
    <message>
        <source>Timelines are resumed.</source>
        <translation>Ленты возобновлены.</translation>
    </message>
</context>
<context>
    <name>YaicsSettingsDialog</name>
    <message>
        <source>Yaics restart is mandatory for settings to take effect.
Please restart Yaics.</source>
        <translation>Для применения настроек необходим перезапуск Yaics.
Пожалуйста, перезапустите Yaics.</translation>
    </message>
</context>
</TS>
