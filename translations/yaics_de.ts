<?xml version="1.0" encoding="utf-8"?>
<!-- German translation by Vinzenz Vietzke <vinz at vinzv.de>-->
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>DirectMessageWindow</name>
    <message>
        <source>Write a direct message to </source>
        <translation>Schreibe Direktnachricht an </translation>
    </message>
    <message>
        <source>Send direct message</source>
        <translation>Sende Direktnachricht</translation>
    </message>
</context>
<context>
    <name>MessageWidget</name>
    <message>
        <source>reply</source>
        <translation>Antworten</translation>
    </message>
    <message>
        <source>direct</source>
        <translation>Direktnachricht</translation>
    </message>
    <message>
        <source>conversation</source>
        <translation>Gespräch</translation>
    </message>
    <message>
        <source> from </source>
        <translation> von </translation>
    </message>
    <message>
        <source>in context</source>
        <translation>im Kontext</translation>
    </message>
    <message>
        <source>in reply to </source>
        <translation>als Antwort auf </translation>
    </message>
    <message>
        <source>unknown</source>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <source>[repeated by %1]</source>
        <translation>[wiederholt von %1]</translation>
    </message>
    <message>
        <source>Mark notice as favourite</source>
        <translation>Nachricht als Favorit markieren</translation>
    </message>
    <message>
        <source>Repeat notice</source>
        <translation>Nachricht wiederholen</translation>
    </message>
    <message>
        <source>Reply to notice</source>
        <translation>Auf Nachricht antworten</translation>
    </message>
    <message>
        <source>Send direct message</source>
        <translation>Direktnachricht senden</translation>
    </message>
    <message>
        <source>Open conversation tab</source>
        <translation>Gespräch in Tab öffnen</translation>
    </message>
</context>
<context>
    <name>MessageWindow</name>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Send message</source>
        <translation>Nachricht senden</translation>
    </message>
    <message>
        <source>Sending ...</source>
        <translation>Sende ...</translation>
    </message>
</context>
<context>
    <name>Prefs</name>
    <message>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>&amp;Main</source>
        <translation>&amp;Standard</translation>
    </message>
    <message>
        <source>Update interval (in minutes):  </source>
        <translation>Updateintervall (in Minuten):  </translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Benachrichtigungen</translation>
    </message>
    <message>
        <source>Home:</source>
        <translation>Start:</translation>
    </message>
    <message>
        <source>Mentions:</source>
        <translation>Erwähnungen:</translation>
    </message>
    <message>
        <source>Do nothing</source>
        <translation>Nichts tun</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Popup notification</source>
        <translation>Benachrichtigungsfenster</translation>
    </message>
    <message>
        <source>Filtering</source>
        <translation>Filtern</translation>
    </message>
    <message>
        <source>Text to filter:</source>
        <translation>Zu filternder Text:</translation>
    </message>
    <message>
        <source>Enclose in /../ for regular expression.</source>
        <translation>Für reguläre Ausdrücke in /../ setzen.</translation>
    </message>
    <message>
        <source>Prepend replies with @username.</source>
        <translation>Antworten @username voranstellen.</translation>
    </message>
    <message>
        <source>Show icon in system tray.</source>
        <translation>Symbol im Benachrichtigungsfeld zeigen.</translation>
    </message>
    <message>
        <source>Select theme:</source>
        <translation>Thema wählen:</translation>
    </message>
    <message>
        <source>Set link colour</source>
        <translation>Linkfarbe festlegen</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <source>Enable &apos;Public&apos; tab.</source>
        <translation>&apos;Öffentlich&apos; Tab aktivieren.</translation>
    </message>
    <message>
        <source>A&amp;ccount</source>
        <translation>A&amp;ccount</translation>
    </message>
    <message>
        <source>Username:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <source>Ignore SSL warnings</source>
        <translation>SSL Warnungen ignorieren</translation>
    </message>
    <message>
        <source>P&amp;roxy</source>
        <translation>P&amp;roxy</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Keiner</translation>
    </message>
    <message>
        <source>Proxy type:</source>
        <translation>Proxytyp:</translation>
    </message>
    <message>
        <source>IP:</source>
        <translation>IP:</translation>
    </message>
    <message>
        <source>Port (TOR port is 9150):</source>
        <translation>Port (TOR Port ist 9150):</translation>
    </message>
    <message>
        <source>Socks5</source>
        <translation>Socks5</translation>
    </message>
    <message>
        <source>HTTP</source>
        <translation>HTTP</translation>
    </message>
    <message>
        <source>GNU social Instance:</source>
        <translation>GNU social Instanz:</translation>
    </message>
</context>
<context>
    <name>StatusWindow</name>
    <message>
        <source>Write a new notice</source>
        <translation>Neue Nachricht schreiben</translation>
    </message>
    <message>
        <source>Write a reply to %1</source>
        <translation></translation>
    </message>
    <message>
        <source>Send notice</source>
        <translation>Nachricht senden</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <source>Updated</source>
        <translation>Aktuell</translation>
    </message>
    <message>
        <source>Loading ...</source>
        <translation>Lade ...</translation>
    </message>
    <message>
        <source>Load older </source>
        <translation>Ältere laden </translation>
    </message>
    <message>
        <source>message</source>
        <translation>Nachricht</translation>
    </message>
    <message>
        <source>update</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <source>messages</source>
        <translation>Nachrichten</translation>
    </message>
    <message>
        <source>updates</source>
        <translation>Updates</translation>
    </message>
</context>
<context>
    <name>YaicsApp</name>
    <message>
        <source>&amp;Public</source>
        <translation>&amp;Öffentlich</translation>
    </message>
    <message>
        <source>&amp;Home</source>
        <translation>&amp;Start</translation>
    </message>
    <message>
        <source>&amp;Mentions</source>
        <translation>&amp;Erwähnungen</translation>
    </message>
    <message>
        <source>P&amp;rofile</source>
        <translation>P&amp;rofil</translation>
    </message>
    <message>
        <source>&amp;Inbox</source>
        <translation>&amp;Eingang</translation>
    </message>
    <message>
        <source>&amp;Outbox</source>
        <translation>&amp;Ausgang</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>V&amp;erlassen</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>&amp;Reload timeline</source>
        <translation>Timeline aktualisie&amp;ren</translation>
    </message>
    <message>
        <source>Load &amp;older in timeline</source>
        <translation>&amp;Ältere in der Timeline laden</translation>
    </message>
    <message>
        <source>&amp;Pause home timeline</source>
        <translation>&amp;Pausiere Start Timeline</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <source>New &amp;direct message</source>
        <translation>Neue &amp;Direktnachricht</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <source>&lt;p&gt;Yaics is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;Yaics is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.&lt;/p&gt;&lt;p&gt;You should have received a copy of the GNU General Public License along with Yaics.  If not, see &lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;http://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Yaics ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiterverbreiten und/oder modifizieren.&lt;/p&gt;&lt;p&gt;Yaics wird in der Hoffnung, dass es nützlich sein wird, aber OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK. Siehe die GNU General Public License für weitere Details.&lt;/p&gt;&lt;p&gt;Sie sollten eine Kopie der GNU General Public License zusammen mit diesem Programm erhalten haben. Wenn nicht, siehe &lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;http://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Copyright &amp;copy; 2010-2015 Mats Sj&amp;ouml;berg</source>
        <translation>Copyright &amp;copy; 2010-2015 Mats Sj&amp;ouml;berg</translation>
    </message>
    <message>
        <source>Copyright &amp;copy; 2014-2015 Stig Atle Steffensen</source>
        <translation>Copyright &amp;copy; 2014-2015 Stig Atle Steffensen</translation>
    </message>
    <message>
        <source>&lt;p&gt;Report bugs and feature requests at </source>
        <translation>&lt;p&gt;Melde Fehler und Wünsche unter </translation>
    </message>
    <message>
        <source>A simple Qt-based GNU social client.</source>
        <translation>Ein einfacher Qt-basierter GNU social Client.</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation>Über %1</translation>
    </message>
    <message>
        <source>&amp;New notice</source>
        <translation>&amp;Neue Nachricht</translation>
    </message>
    <message>
        <source>Conversation</source>
        <translation>Gespräch</translation>
    </message>
    <message>
        <source>&amp;Bugtracker</source>
        <translation>&amp;Bugtracker</translation>
    </message>
    <message>
        <source>Timelines are paused.</source>
        <translation>Timelines sind pausiert.</translation>
    </message>
    <message>
        <source>Timelines are resumed.</source>
        <translation>Timelines sind wieder aktiv.</translation>
    </message>
</context>
<context>
    <name>YaicsSettingsDialog</name>
    <message>
        <source>Yaics restart is mandatory for settings to take effect.
Please restart Yaics.</source>
        <translation>Ein Neustart von Yaics ist erforderlich um die Einstellungen zu akvieren.
Bitte start Yaics neu.</translation>
    </message>
</context>
</TS>
