<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>DirectMessageWindow</name>
    <message>
        <source>Write a direct message to </source>
        <translation></translation>
    </message>
    <message>
        <source>Send direct message</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MessageWidget</name>
    <message>
        <source>reply</source>
        <translation></translation>
    </message>
    <message>
        <source>direct</source>
        <translation></translation>
    </message>
    <message>
        <source>conversation</source>
        <translation></translation>
    </message>
    <message>
        <source> from </source>
        <translation></translation>
    </message>
    <message>
        <source>in context</source>
        <translation></translation>
    </message>
    <message>
        <source>in reply to </source>
        <translation></translation>
    </message>
    <message>
        <source>unknown</source>
        <translation></translation>
    </message>
    <message>
        <source>[repeated by %1]</source>
        <translation></translation>
    </message>
    <message>
        <source>Mark notice as favourite</source>
        <translation></translation>
    </message>
    <message>
        <source>Repeat notice</source>
        <translation></translation>
    </message>
    <message>
        <source>Reply to notice</source>
        <translation></translation>
    </message>
    <message>
        <source>Send direct message</source>
        <translation></translation>
    </message>
    <message>
        <source>Open conversation tab</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MessageWindow</name>
    <message>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <source>Send message</source>
        <translation></translation>
    </message>
    <message>
        <source>Sending ...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Prefs</name>
    <message>
        <source>Preferences</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;Main</source>
        <translation></translation>
    </message>
    <message>
        <source>Update interval (in minutes):  </source>
        <translation></translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation></translation>
    </message>
    <message>
        <source>Home:</source>
        <translation></translation>
    </message>
    <message>
        <source>Mentions:</source>
        <translation></translation>
    </message>
    <message>
        <source>Do nothing</source>
        <translation></translation>
    </message>
    <message>
        <source>Highlight tray icon</source>
        <translation></translation>
    </message>
    <message>
        <source>Popup notification</source>
        <translation></translation>
    </message>
    <message>
        <source>Filtering</source>
        <translation></translation>
    </message>
    <message>
        <source>Text to filter:</source>
        <translation></translation>
    </message>
    <message>
        <source>Enclose in /../ for regular expression.</source>
        <translation></translation>
    </message>
    <message>
        <source>Prepend replies with @username.</source>
        <translation></translation>
    </message>
    <message>
        <source>Show icon in system tray.</source>
        <translation></translation>
    </message>
    <message>
        <source>Select theme:</source>
        <translation></translation>
    </message>
    <message>
        <source>Set link colour</source>
        <translation></translation>
    </message>
    <message>
        <source>Default</source>
        <translation></translation>
    </message>
    <message>
        <source>Enable &apos;Public&apos; tab.</source>
        <translation></translation>
    </message>
    <message>
        <source>A&amp;ccount</source>
        <translation></translation>
    </message>
    <message>
        <source>Username:</source>
        <translation></translation>
    </message>
    <message>
        <source>Password:</source>
        <translation></translation>
    </message>
    <message>
        <source>Ignore SSL warnings</source>
        <translation></translation>
    </message>
    <message>
        <source>P&amp;roxy</source>
        <translation></translation>
    </message>
    <message>
        <source>None</source>
        <translation></translation>
    </message>
    <message>
        <source>Proxy type:</source>
        <translation></translation>
    </message>
    <message>
        <source>IP:</source>
        <translation></translation>
    </message>
    <message>
        <source>Port (TOR port is 9150):</source>
        <translation></translation>
    </message>
    <message>
        <source>Socks5</source>
        <translation></translation>
    </message>
    <message>
        <source>HTTP</source>
        <translation></translation>
    </message>
    <message>
        <source>GNU social Instance:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>StatusWindow</name>
    <message>
        <source>Write a new notice</source>
        <translation></translation>
    </message>
    <message>
        <source>Write a reply to %1</source>
        <translation></translation>
    </message>
    <message>
        <source>Send notice</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <source>Updated</source>
        <translation></translation>
    </message>
    <message>
        <source>Loading ...</source>
        <translation></translation>
    </message>
    <message>
        <source>Load older </source>
        <translation></translation>
    </message>
    <message>
        <source>message</source>
        <translation></translation>
    </message>
    <message>
        <source>update</source>
        <translation></translation>
    </message>
    <message>
        <source>messages</source>
        <translation></translation>
    </message>
    <message>
        <source>updates</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>YaicsApp</name>
    <message>
        <source>&amp;Public</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;Home</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;Mentions</source>
        <translation></translation>
    </message>
    <message>
        <source>P&amp;rofile</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;Inbox</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;Outbox</source>
        <translation></translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation></translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;Reload timeline</source>
        <translation></translation>
    </message>
    <message>
        <source>Load &amp;older in timeline</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;Pause home timeline</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation></translation>
    </message>
    <message>
        <source>New &amp;direct message</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;p&gt;Yaics is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;Yaics is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.&lt;/p&gt;&lt;p&gt;You should have received a copy of the GNU General Public License along with Yaics.  If not, see &lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;http://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>Copyright &amp;copy; 2010-2015 Mats Sj&amp;ouml;berg</source>
        <translation></translation>
    </message>
    <message>
        <source>Copyright &amp;copy; 2014-2015 Stig Atle Steffensen</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;p&gt;Report bugs and feature requests at </source>
        <translation></translation>
    </message>
    <message>
        <source>A simple Qt-based GNU social client.</source>
        <translation></translation>
    </message>
    <message>
        <source>About %1</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;New notice</source>
        <translation></translation>
    </message>
    <message>
        <source>Conversation</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;Bugtracker</source>
        <translation></translation>
    </message>
    <message>
        <source>Timelines are paused.</source>
        <translation></translation>
    </message>
    <message>
        <source>Timelines are resumed.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>YaicsSettingsDialog</name>
    <message>
        <source>Yaics restart is mandatory for settings to take effect.
Please restart Yaics.</source>
        <translation></translation>
    </message>
</context>
</TS>
