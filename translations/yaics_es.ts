<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>DirectMessageWindow</name>
    <message>
        <source>Write a direct message to </source>
        <translation>Envía un mensaje directo a </translation>
    </message>
    <message>
        <source>Send direct message</source>
        <translation>Enviar un mensaje directo</translation>
    </message>
</context>
<context>
    <name>MessageWidget</name>
    <message>
        <source>reply</source>
        <translation>responder</translation>
    </message>
    <message>
        <source>direct</source>
        <translation>directo</translation>
    </message>
    <message>
        <source>conversation</source>
        <translation>conversación</translation>
    </message>
    <message>
        <source> from </source>
        <translation> de </translation>
    </message>
    <message>
        <source>in context</source>
        <translation>en contexto</translation>
    </message>
    <message>
        <source>in reply to </source>
        <translation>respuesta a </translation>
    </message>
    <message>
        <source>unknown</source>
        <translation>desconocido</translation>
    </message>
    <message>
        <source>[repeated by %1]</source>
        <translation>[repetido por %1]</translation>
    </message>
    <message>
        <source>Mark notice as favourite</source>
        <translation>Marcar el aviso como favorito</translation>
    </message>
    <message>
        <source>Repeat notice</source>
        <translation>Repetir el aviso</translation>
    </message>
    <message>
        <source>Reply to notice</source>
        <translation>Responder al aviso</translation>
    </message>
    <message>
        <source>Send direct message</source>
        <translation>Enviar un mensaje directo</translation>
    </message>
    <message>
        <source>Open conversation tab</source>
        <translation>Abrir pestaña de conversación</translation>
    </message>
</context>
<context>
    <name>MessageWindow</name>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <source>Send message</source>
        <translation>Enviar mensaje</translation>
    </message>
    <message>
        <source>Sending ...</source>
        <translation>Enviando…</translation>
    </message>
</context>
<context>
    <name>Prefs</name>
    <message>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <source>&amp;Main</source>
        <translation>&amp;Principal</translation>
    </message>
    <message>
        <source>Update interval (in minutes):  </source>
        <translation>Intervalo de actualización (minutos):  </translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notificaciones</translation>
    </message>
    <message>
        <source>Home:</source>
        <translation>Inicio:</translation>
    </message>
    <message>
        <source>Mentions:</source>
        <translation>Menciones:</translation>
    </message>
    <message>
        <source>Do nothing</source>
        <translation>No hacer nada</translation>
    </message>
    <message>
        <source>Highlight tray icon</source>
        <translation>Aviso en la bandeja del sistema</translation>
    </message>
    <message>
        <source>Popup notification</source>
        <translation>Notificación emergente</translation>
    </message>
    <message>
        <source>Filtering</source>
        <translation>Filtrado</translation>
    </message>
    <message>
        <source>Text to filter:</source>
        <translation>Texto para filtrar:</translation>
    </message>
    <message>
        <source>Enclose in /../ for regular expression.</source>
        <translation>Rodear con /../ para expresión regular.</translation>
    </message>
    <message>
        <source>Prepend replies with @username.</source>
        <translation>Anteponer respuestas con @usuario.</translation>
    </message>
    <message>
        <source>Show icon in system tray.</source>
        <translation>Mostrar icono en la bandeja del sistema.</translation>
    </message>
    <message>
        <source>Select theme:</source>
        <translation>Seleccionar tema:</translation>
    </message>
    <message>
        <source>Set link colour</source>
        <translation>Establecer color de enlace</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Omisión</translation>
    </message>
    <message>
        <source>Enable &apos;Public&apos; tab.</source>
        <translation>Habilitar &apos;Público&apos;.</translation>
    </message>
    <message>
        <source>A&amp;ccount</source>
        <translation>C&amp;uenta</translation>
    </message>
    <message>
        <source>Username:</source>
        <translation>Usuario:</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Clave:</translation>
    </message>
    <message>
        <source>Ignore SSL warnings</source>
        <translation>Ignorar los avisos de SSL</translation>
    </message>
    <message>
        <source>P&amp;roxy</source>
        <translation>P&amp;roxy</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <source>Proxy type:</source>
        <translation>Tipo de proxy:</translation>
    </message>
    <message>
        <source>IP:</source>
        <translation>IP:</translation>
    </message>
    <message>
        <source>Port (TOR port is 9150):</source>
        <translation>Puerto (El puerto de TOR es 9150):</translation>
    </message>
    <message>
        <source>Socks5</source>
        <translation></translation>
    </message>
    <message>
        <source>HTTP</source>
        <translation></translation>
    </message>
    <message>
        <source>GNU social Instance:</source>
        <translation>Servidor de GNU social:</translation>
    </message>
</context>
<context>
    <name>StatusWindow</name>
    <message>
        <source>Write a reply to %1</source>
        <translation>Responder a %1</translation>
    </message>
    <message>
        <source>Send notice</source>
        <translation>Enviar aviso</translation>
    </message>
    <message>
        <source>Write a new notice</source>
        <translation>Escribir un aviso nuevo</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <source>Updated</source>
        <translation>Actualizado</translation>
    </message>
    <message>
        <source>Loading ...</source>
        <translation>Cargando…</translation>
    </message>
    <message>
        <source>Load older </source>
        <translation>Cargar más antiguos </translation>
    </message>
    <message>
        <source>message</source>
        <translation>mensaje</translation>
    </message>
    <message>
        <source>update</source>
        <translation>actualizar</translation>
    </message>
    <message>
        <source>messages</source>
        <translation>mensajes</translation>
    </message>
    <message>
        <source>updates</source>
        <translation>actualizaciones</translation>
    </message>
</context>
<context>
    <name>YaicsApp</name>
    <message>
        <source>E&amp;xit</source>
        <translation>S&amp;alir</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <source>&amp;Reload timeline</source>
        <translation>&amp;Recargar la línea temporal</translation>
    </message>
    <message>
        <source>Load &amp;older in timeline</source>
        <translation>Cargar &amp;más antiguos en la línea temporal</translation>
    </message>
    <message>
        <source>&amp;Pause home timeline</source>
        <translation>&amp;Detener la línea temporal inicial</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;Acerca de</translation>
    </message>
    <message>
        <source>New &amp;direct message</source>
        <translation>Nuevo &amp;mensaje directo</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <source>&lt;p&gt;Yaics is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;Yaics is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.&lt;/p&gt;&lt;p&gt;You should have received a copy of the GNU General Public License along with Yaics.  If not, see &lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;http://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Yaics es software libre: puede ser redistribuirlo y modificarlo cumpliendo los términos de la Licencia Pública General GNU publicada por la Free Software Foundation, versión 3 o, a tu elección, cualquier versión posterior.&lt;/p&gt;&lt;p&gt;Yaics se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA; ni siquiera la garantía tácita MERCANTIL o de APTITUD PARA UN PROPÓSITO DETERMINADO. Consulta la Licencia Pública General GNU para conocer más detalles.&lt;/p&gt;&lt;p&gt;Junto con Yaics, deberías haber recibido una copia de la Licencia Pública General GNU. En caso contrario, puedes consultar &lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;http://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Copyright &amp;copy; 2010-2015 Mats Sj&amp;ouml;berg</source>
        <translation></translation>
    </message>
    <message>
        <source>Copyright &amp;copy; 2014-2015 Stig Atle Steffensen</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;p&gt;Report bugs and feature requests at </source>
        <translation>&lt;p&gt;Comunicación de errores y petición de características a</translation>
    </message>
    <message>
        <source>A simple Qt-based GNU social client.</source>
        <translation>Sencillo cliente Qt para GNU social.</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation>Acerca de %1</translation>
    </message>
    <message>
        <source>&amp;Public</source>
        <translation>&amp;Público</translation>
    </message>
    <message>
        <source>&amp;Home</source>
        <translation>&amp;Inicio</translation>
    </message>
    <message>
        <source>&amp;Mentions</source>
        <translation>&amp;Menciones</translation>
    </message>
    <message>
        <source>P&amp;rofile</source>
        <translation>P&amp;erfil</translation>
    </message>
    <message>
        <source>&amp;Inbox</source>
        <translation>&amp;Entrada</translation>
    </message>
    <message>
        <source>&amp;Outbox</source>
        <translation>&amp;Enviados</translation>
    </message>
    <message>
        <source>&amp;New notice</source>
        <translation>&amp;Aviso nuevo</translation>
    </message>
    <message>
        <source>Conversation</source>
        <translation>Conversación</translation>
    </message>
    <message>
        <source>&amp;Bugtracker</source>
        <translation>&amp;Bugtracker</translation>
    </message>
    <message>
        <source>Timelines are paused.</source>
        <translation>Las lineas temporales están pausadas.</translation>
    </message>
    <message>
        <source>Timelines are resumed.</source>
        <translation>Las lineas temporales están reanudadas.</translation>
    </message>
</context>
<context>
    <name>YaicsSettingsDialog</name>
    <message>
        <source>Yaics restart is mandatory for settings to take effect.
Please restart Yaics.</source>
        <translation>Es necesario reiniciar Yaics para que los ajustes surtan efecto.
Por favor reinicia.</translation>
    </message>
</context>
</TS>
