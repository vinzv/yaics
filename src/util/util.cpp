/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "util.h"
#include <QtGlobal>

#define DOMAIN_REGEXP "http(s|)://(www\\.|)([^/]+)"

//------------------------------------------------------------------------------

QString getXMLElementAttr(const QDomNode& node, const QString& name,
                          const QString& attrname) {
  QDomElement el = node.firstChildElement(name);
  if (!el.isNull())
    return el.attribute(attrname, "");
  return "";
}  

//------------------------------------------------------------------------------

QString getXMLElementText(const QDomNode& node, const QString& name) {
  QDomElement el = node.firstChildElement(name);
  if (!el.isNull())
    return el.text();
  return "";
}

//------------------------------------------------------------------------------

QString ahref(const QString& url, const QString& text) {
  return "<a href=\""+url+"\">"+text+"</a>";
}

//------------------------------------------------------------------------------

QString wrapTag(const QString& tag, const QString& text, const QString& attrs) {
  return QString("<%1%3>%2</%1>").arg(tag).arg(text).arg(attrs.isEmpty()?"":
                                                         " "+attrs);
}

//------------------------------------------------------------------------------

QString slashify(const QString& url) {
  QString ret = url;
  if (!ret.endsWith('/'))
    ret.append('/');
  return ret;
}

//------------------------------------------------------------------------------

QString relativeFuzzyTime(QDateTime sTime) {
  QString dateStr = sTime.toString("ddd d MMMM yyyy");

  int secs = sTime.secsTo(QDateTime::currentDateTime().toUTC());
  if (secs < 0)
    secs = 0;
  int mins = qRound((float)secs/60);
  int hours = qRound((float)secs/60/60);
    
  if (secs < 60) { 
    dateStr = QString("a few seconds ago");
  } else if (mins < 60) {
    dateStr = QString("%1 minute%2 ago").arg(mins).arg(mins==1?"":"s");
  } else if (hours < 24) {
    dateStr = QString("%1 hour%2 ago").arg(hours).arg(hours==1?"":"s");
  }
  return dateStr;
}

//------------------------------------------------------------------------------

QString fuzzyDate(const QString& dateStr) {
  QString ds = dateStr;
  QStringList parts = dateStr.split(" ");
  QRegExp rx("(\\+|\\-)(\\d{2})(\\d{2})");
  int addSecs = 0;
  if (rx.exactMatch(parts[4])) {
    int sign = rx.cap(1) == "-" ? -1 : 1;
    int hour = rx.cap(2).toInt();
    int min  = rx.cap(3).toInt();
    parts.removeAt(4);
    ds = parts.join(" ");
    addSecs = -sign * (hour * 60 + min) * 60;
  }

  QDateTime sTime = QLocale(QLocale::C).toDateTime(ds,
                                                   "ddd MMM d h:mm:ss yyyy");
  sTime.setTimeSpec(Qt::UTC);

  if (addSecs != 0)
    sTime = sTime.addSecs(addSecs);
  
  if (sTime.isValid())
    return relativeFuzzyTime(sTime);

  return dateStr;
}

//------------------------------------------------------------------------------

QString urlToDomain(const QString& url) {
  QRegExp rx(DOMAIN_REGEXP);

  if (rx.indexIn(url) != -1)
    return rx.cap(3);

  return "";
}
