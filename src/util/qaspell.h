/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.
  
  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Yaics is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.  
*/

/** QASpell
  Tries to be a wrapper around libaspell, following this:
  http://aspell.net/man-html/Through-the-C-API.html#Through-the-C-API
**/

#ifndef _QASPELL_H_
#define _QASPELL_H_

#ifdef USE_ASPELL

#include <QtCore>
#include <aspell.h>

class QASpell : public QObject {
public:
  QASpell(QObject* parent=0);

  ~QASpell();

  bool checkWord(const QString& word) const;

protected:
  AspellConfig* spell_config;
  AspellSpeller* spell_checker;
};

#endif // USE_ASPELL

#endif
