/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef YAICSSETTINGS_H
#define YAICSSETTINGS_H

#include <QObject>
#include <QSettings>
#include <qcolor.h>

#define DEFAULT_API_URL "https://quitter.se/"

class YaicsSettings : public QObject {
  Q_OBJECT
  Q_PROPERTY(QString apiURL READ getAPIUrl NOTIFY accountChanged)
  Q_PROPERTY(QString username READ getUsername NOTIFY accountChanged)
  Q_PROPERTY(QString password READ getPassword NOTIFY accountChanged)
  Q_PROPERTY(bool ignoreSSLWarnings READ getIgnoreSSLWarnings NOTIFY accountChanged)
  Q_PROPERTY(bool useApiRepeat READ getUseApiRepeat NOTIFY settingsChanged)
  Q_PROPERTY(QString qtStyle READ getQtStyle NOTIFY settingsChanged)
  Q_PROPERTY(bool enablePublicTimeline READ getEnablePublicTimeline NOTIFY settingsChanged)

  Q_PROPERTY(qint8 proxyType READ getProxyType WRITE setProxyType NOTIFY settingsChanged)
  Q_PROPERTY(QString proxyIP READ getProxyIP WRITE setProxyIP NOTIFY settingsChanged)
  Q_PROPERTY(QString proxyPort READ getProxyPort WRITE setProxyPort NOTIFY settingsChanged)
  Q_PROPERTY(QString proxyUsername READ getProxyUsername WRITE setProxyUsername NOTIFY settingsChanged)
  Q_PROPERTY(QString proxyPassword READ getProxyPassword WRITE setProxyPassword NOTIFY settingsChanged)
  Q_PROPERTY(QString linkColour READ getlinkColour WRITE setlinkColour NOTIFY settingsChanged)

public:
  static YaicsSettings* getSettings(QObject* parent=0);

  void write(QSettings& s);

  void read(QSettings& s);

  quint8 getProxyType() const { return proxyType; }
  void setProxyType(const quint8 s) { proxyType = s; }

  QString getProxyIP() const { return proxyIP; }
  void setProxyIP(const QString& s) { proxyIP = s; }

  QString getProxyPort() const { return proxyPort; }
  void setProxyPort(const QString& s) { proxyPort = s; }

  QString getProxyUsername() const { return proxyUsername; }
  void setProxyUsername(const QString& s) { proxyUsername = s; }

  QString getProxyPassword() const { return proxyPassword; }
  void setProxyPassword(const QString& s) { proxyPassword = s; }

  QString getAPIUrl() const { return apiURL; }
  void setAPIUrl(const QString& s) { apiURL = s; }

  QString getUsername() const { return username; }
  void setUsername(const QString& s) { username = s; }

  QString getPassword() const { return password; }
  void setPassword(const QString& s) { password = s; }

  QString getTextToFilter() const { return textToFilter; }
  void setTextToFilter(const QString& s) { textToFilter = s; }

  int getReloadTime() const { return reloadTime; }
  void setReloadTime(const int i) { reloadTime = i; }

  bool getUseTrayIcon() const { return useTrayIcon; }
  void setUseTrayIcon(bool b) { useTrayIcon = b; }

  bool getUseApiRepeat() const { return useApiRepeat; }
  void setUseApiRepeat(bool b) { useApiRepeat = b; }

  bool getPrependReplies() const { return prependReplies; }
  void setPrependReplies(bool b) { prependReplies = b; }

  enum notifyType { DoNothing=0, HighlightTray=1, Popup=2 };

  notifyType getHomeNotify() const { return homeNotify; }
  void setHomeNotify(notifyType n) { homeNotify = n; }

  notifyType getMentionsNotify() const { return mentionsNotify; }
  void setMentionsNotify(notifyType n) { mentionsNotify = n; }

  bool getIgnoreSSLWarnings() const { return ignoreSSLWarnings; }
  void setIgnoreSSLWarnings(bool b) { ignoreSSLWarnings = b; }

  QString getQtStyle() const{ return qtStyle;}
  void setQtStyle(const QString& s) { qtStyle = s; }

  bool getEnablePublicTimeline() const { return enablePublicTimeline; }
  void setEnablePublicTimeline(bool b) { enablePublicTimeline = b; }

  bool getshowAttachment() const {return showAttachment;}
  void setshowAttachment(bool b) { showAttachment = b;}

  QString getlinkColour() const {return linkColour;}
  void setlinkColour(QString& c){linkColour = c;}

signals:
  void accountChanged();
  void settingsChanged();
  void filterChanged();

private:
  YaicsSettings(QObject* parent=0);

  QString apiURL;
  QString username;
  QString password;

  QString textToFilter;

  int reloadTime;

  bool useTrayIcon;

  bool useApiRepeat;
  QString qtStyle;
  bool prependReplies;
  bool enablePublicTimeline;
  bool showAttachment;
  QString proxyIP;
  QString proxyPort;
  QString proxyUsername;
  QString proxyPassword;
  qint8 proxyType;
  QString linkColour;

  static YaicsSettings* settings;

  bool ignoreSSLWarnings;
  notifyType homeNotify;
  notifyType mentionsNotify;
};

#endif
