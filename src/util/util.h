/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UTIL_H
#define UTIL_H

#include <QtXml>

QString getXMLElementAttr(const QDomNode& node, const QString& name,
                          const QString& attrname);

QString getXMLElementText(const QDomNode& node, const QString& name);

QString ahref(const QString& url, const QString& text);

QString wrapTag(const QString& tag, const QString& text,
                const QString& attrs="");

QString slashify(const QString& url);

QString relativeFuzzyTime(QDateTime);

QString fuzzyDate(const QString&);

QString urlToDomain(const QString&);

#endif
