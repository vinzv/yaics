/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "yaicssettings.h"
#include "util.h"

//------------------------------------------------------------------------------

YaicsSettings* YaicsSettings::settings = NULL;

//------------------------------------------------------------------------------

YaicsSettings* YaicsSettings::getSettings(QObject* parent) {
  if (!settings)
    settings = new YaicsSettings(parent);
  return settings;
}  

//------------------------------------------------------------------------------

YaicsSettings::YaicsSettings(QObject* parent) : QObject(parent) {
}

//------------------------------------------------------------------------------

void YaicsSettings::write(QSettings& s) {
  QFile::setPermissions(s.fileName(), QFile::ReadOwner | QFile::WriteOwner);

  s.beginGroup("General");
  s.setValue("qtstyle", qtStyle);
  s.setValue("reload_time", reloadTime);
  s.setValue("use_tray_icon", useTrayIcon);
  s.setValue("text_to_filter", textToFilter);
  s.setValue("use_api_repeat", useApiRepeat);
  s.setValue("prepend_replies", prependReplies);
  s.setValue("enable_public_timeline", enablePublicTimeline);
  s.setValue("showAttachment", showAttachment);
  s.setValue("linkColour", linkColour);
  s.endGroup();

  s.beginGroup("Notify");
  s.setValue("homeNotify", (int)homeNotify);
  s.setValue("mentionsNotify", (int)mentionsNotify);
  s.endGroup();

  s.beginGroup("Account");
  s.setValue("api_url", apiURL);
  s.setValue("username", username);

  s.setValue("password", password.toUtf8().toBase64().constData());
  s.setValue("ignoreSSLWarnings", ignoreSSLWarnings);
  s.endGroup();

  s.beginGroup("Proxy");
  s.setValue("proxy_type", proxyType);
  s.setValue("proxy_ip", proxyIP);
  s.setValue("proxy_port", proxyPort);
  s.setValue("proxy_username", proxyUsername);
  s.setValue("proxy_password", proxyPassword);
  s.endGroup();
}

//------------------------------------------------------------------------------

void YaicsSettings::read(QSettings& s) {
  s.beginGroup("General");
  qtStyle = s.value("qtstyle","").toString();
  reloadTime = s.value("reload_time", 5).toInt();
  if (reloadTime < 1)
    reloadTime = 1;
  useTrayIcon = s.value("use_tray_icon", true).toBool();
  textToFilter = s.value("text_to_filter", "").toString();
  useApiRepeat = s.value("use_api_repeat", true).toBool();
  prependReplies = s.value("prepend_replies", true).toBool();
  enablePublicTimeline = s.value("enable_public_timeline",true).toBool();
  showAttachment = s.value("showAttachment",false).toBool();
  linkColour = s.value("linkColour","").toString();
  s.endGroup();

  s.beginGroup("Notify");
  homeNotify = (notifyType)s.value("homeNotify",0).toInt();
  mentionsNotify = (notifyType)s.value("mentionsNotify",1).toInt();
  s.endGroup();

  s.beginGroup("Account");
  apiURL = slashify(s.value("api_url", DEFAULT_API_URL).toString());
  username = s.value("username", "").toString();

  QByteArray passwordBA =
    QByteArray::fromBase64(s.value("password", "").toByteArray());
  password = passwordBA.constData();

  ignoreSSLWarnings = s.value("ignoreSSLWarnings", false).toBool();
  s.endGroup();

  s.beginGroup("Proxy");
  proxyIP = s.value("proxy_ip","").toString();
  proxyPassword = s.value("proxy_password","").toString();
  proxyPort = s.value("proxy_port","").toString();
  proxyType = s.value("proxy_type","").toInt();
  proxyUsername = s.value("proxy_username","").toString();
  s.endGroup();
}
