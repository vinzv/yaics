/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "shorturlexpander.h"
#include "util.h"

//------------------------------------------------------------------------------

QMap<QString,QString> ShortUrlExpander::m_longUrl;

//------------------------------------------------------------------------------

ShortUrlExpander::ShortUrlExpander(const QString& url, QObject* parent) :
  QObject(parent), m_url(url)
{
  m_nam = new QNetworkAccessManager(this);
  connect(m_nam, SIGNAL(finished(QNetworkReply*)),
          this, SLOT(onFinished(QNetworkReply*)));
}

//------------------------------------------------------------------------------

QString ShortUrlExpander::longUrl() {
  // Check if url is already in static map of shorturl => longurl 
  if (m_longUrl.contains(m_url)) {
    const QString& lurl = m_longUrl.value(m_url);
    if (lurl.isEmpty()) // we are already fetching it
      return m_url; 
    else                                // return previously fetched long url
      return lurl;
  }

  // otherwise, start fetching if it is one of the know short urls
  QRegExp shortUrlRX(shortUrls);
  if (m_url.contains(shortUrlRX)) {
    QNetworkRequest req;
    req.setUrl(QUrl(m_url));
    // req.setRawHeader("User-Agent", "Mozilla/4.0");
    // req.attribute(QNetworkRequest::HttpPipeliningAllowedAttribute, false);
    m_nam->head(req);

    // we insert an empty string into the map as a signal that we are
    // currently fetching the url
    m_longUrl.insert(m_url, ""); 
  }
  return m_url;
}

//------------------------------------------------------------------------------

void ShortUrlExpander::onFinished(QNetworkReply* nr) {
  bool debug = false;

  if (nr->error()) {
    emit error("Network error: "+nr->errorString());
    return;
  }

  QVariant loc = nr->header(QNetworkRequest::LocationHeader);

  if (loc.isValid() && loc.canConvert(QVariant::Url)) {
    QByteArray newUrlBA = loc.toUrl().toEncoded();
    QString newUrl(newUrlBA.constData());
    m_longUrl.insert(m_url, newUrl);
    emit urlReady(m_url, newUrl);
  } else {
    qDebug() << "ShortUrlExpander: got bad response for" << m_url;
    if (debug) {
      QList<QByteArray> hl = nr->rawHeaderList();
      for (int i=0; i<hl.size(); ++i)
        qDebug() << "HEADER" << QString(hl.at(i)) << "=>"
                 << QString(nr->rawHeader(hl.at(i)));
    }
    m_longUrl.remove(m_url);
  }
  nr->deleteLater();
}
