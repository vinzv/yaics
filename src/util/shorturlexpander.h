/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SHORTURLEXPANDER_H
#define SHORTURLEXPANDER_H

#include <QtCore>
#include <QtNetwork>

#include "yaicssettings.h"

static const QString shortUrls = 
  "goo.gl/|ur1.ca/|is.gd/|bit.ly/|tinyurl.com/|pnt.me/|3.ly/|ur.ly/|deb.li/|"
  "b1t.it/|nd.gd/|j.mp/|t.co/|2tu.us/|pny.lv/";

/* Example usage:
   ShortUrlExpander* sue = new ShortUrlExpander(myShortUrl, this);
   connect(sue, SIGNAL(urlReady(const QString&, const QString&)),
           this, SLOT(onUrlReady(const QString&, const QString&)));

   // returns short url if not yet ready...
   QString myLongUrl = sue->longUrl();
 */

class ShortUrlExpander : public QObject {
  Q_OBJECT

public:
  ShortUrlExpander(const QString& url, QObject* parent=0);
  
  QString longUrl();

signals:
  void error(const QString&);
  void urlReady(const QString&, const QString&);

private slots:
  void onFinished(QNetworkReply*);

private:
  // void get(const QString& res);
  
  QNetworkAccessManager* m_nam;
  QString m_url;
  static QMap<QString,QString> m_longUrl;
};

#endif
