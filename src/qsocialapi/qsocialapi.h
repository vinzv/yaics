/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _QSOCIALAPI_H_
#define _QSOCIALAPI_H_

#include <QObject>
#include <QtNetwork>
#include <exception>

#include "status.h"
#include "messagelist.h"
#include "directmessage.h"
#include "qsocialapirequest.h"

//------------------------------------------------------------------------------

class QSocialAPI : public QObject {
  Q_OBJECT

public:
  QSocialAPI(QObject* parent=0);
  virtual ~QSocialAPI();

  void setAPIUrl(QString url);
  void setUsername(QString username);
  void setPassword(QString password);
  void setClientSource(QString clientSource);
  void setIgnoreSSLErrors(bool);

  QString getAPIUrl() const { return APIUrl; }
  QString getUsername() const { return username; }

  QSocialAPIRequest* getServerConfig();

  QSocialAPIRequest* timelineRequest(QString path, message_id_t=-1, bool=false);
  QSocialAPIRequest* postUpdate(QString msg, message_id_t in_reply_to_id=-1);
  QSocialAPIRequest* postDirectMessage(QString msg, User* recipient);
  QSocialAPIRequest* getUser(user_id_t id, QString screenName);
  QSocialAPIRequest* favourite(bool set, message_id_t id);
  QSocialAPIRequest* friends();
  QSocialAPIRequest* groups();
  QSocialAPIRequest* directMessages();
  QSocialAPIRequest* sentDirectMessages();
  QSocialAPIRequest* repeatStatus(message_id_t);
  QSocialAPIRequest* verifyCredentials();
  QSocialAPIRequest* friendicaPing();

  QList<User*> getCurrentUsers() const {
    return users.values();
  }

  User* getCurrentUser(user_id_t id) const {
    return users.contains(id) ? users[id] : NULL;
  }

  StatusMessage* getStatus(message_id_t id) const {
    return statuses.contains(id) ? statuses[id] : NULL;
  }

  DirectMessage* getDM(message_id_t id) const {
    return dms.contains(id) ? dms[id] : NULL;
  }

  void executeRequest(QSocialAPIRequest*);

  bool isLoading() const { return replyToRequest.count() > 0; }

  bool isFriendica() const { return friendica; }

  QString getConfig(const QString& key) const;
  
  int getTextLimit() const;

  int getHttpStatusCode(QSocialAPIRequest* rq) const;

signals:
  void error(QString);
  void loadingChanged();                     

protected slots:
  void onFinished(QNetworkReply*);
  void onAuthenticationRequired(QNetworkReply*, QAuthenticator*);
  void onSslErrors(QNetworkReply*, const QList<QSslError>&);

private:
  void parsePing(const QDomDocument& doc);
  User* parseUser(const QDomNode& node);
  StatusMessage* parseStatus(const QDomNode& node);
  DirectMessage* parseDirectMessage(const QDomNode& node);

  QSocialAPIConfig* parseConfig(const QDomNode&);

  MessageList parseTimeline(const QDomDocument&);
  UserList parseFriends(const QDomDocument&);
  QStringList parseGroups(const QDomDocument&);
  MessageList parseDirectMessages(const QDomDocument&);

  QString APIUrl;
  QString username;
  QString password;

  QString clientSource;
  bool ignoreSSLErrors;

  QNetworkAccessManager* net;
  QNetworkCookieJar* cookies;
  QMap<QNetworkReply*, QSocialAPIRequest*> replyToRequest;

  QHash<message_id_t, StatusMessage*> statuses;
  QHash<user_id_t, User*> users;
  QHash<user_id_t, DirectMessage*> dms;

  QSocialAPIConfig* config;
  bool friendica;
};

#endif
