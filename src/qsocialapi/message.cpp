/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "message.h"
#include "util/util.h"

//------------------------------------------------------------------------------

Message::Message(const QDomNode& node, QObject* parent) :
  QObject(parent), user(NULL) {
  setFromXML(node);
}

//------------------------------------------------------------------------------

Message::Message(message_id_t nId, const QString& nText) :
  id(nId), plainText(nText)
{}

//------------------------------------------------------------------------------

QString Message::getSource() const {
  if (source == "statusnet" && user) {
    QString domain = user->getDomain();
    if (!domain.isEmpty())
      return domain;
  }
  return source;
}


//------------------------------------------------------------------------------

QString Message::getFuzzyDate() const {
  return fuzzyDate(date);
}

//------------------------------------------------------------------------------

void Message::setFromXML(const QDomNode& node) {
  // Print xml for debugging
  // QTextStream ss(stdout);
  // node.save(ss,2);

  // FIXME: qFatals should be exceptions or sumfin

  id = getXMLId(node);
  if (id == -1)
    qFatal("No id.");

  source = getXMLElementText(node, "source");
}

//------------------------------------------------------------------------------

message_id_t Message::getXMLId(const QDomNode& node) {
  bool ok;
  message_id_t id = getXMLElementText(node, "id").toLong(&ok);
  if (!ok)
    id = -1;
  return id;
}
