/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "messagemodel.h"
#include "status.h"
#include "util/util.h"

//------------------------------------------------------------------------------

MessageModel::MessageModel(QSocialAPI* os, QObject* parent, bool oldestFirst) :
  QAbstractListModel(parent),
  m_loading(false),
  m_os(os),
  m_oldestFirst(oldestFirst)
{
  m_roles[IdRole] = "statusId";
  m_roles[UserIdRole] = "userId";
  m_roles[UserRole] = "userScreenName";
  m_roles[UserFullRole] = "userFullName";
  m_roles[DateRole] = "date";
  m_roles[SourceRole] = "source";
  m_roles[TextRole] = "statusText";
  m_roles[PathRole] = "path";
  m_roles[RepeatRole] = "repeat";
  m_roles[InReplyToRole] = "inReplyTo";
  m_roles[ImageFileNameRole] = "imageFileName";
  m_roles[FavouritedRole] = "favourited";
}

//------------------------------------------------------------------------------

void MessageModel::reset(QSocialAPI* os) {
  clear();
  m_os = os;
}

//------------------------------------------------------------------------------

void MessageModel::setLoading(bool loading) {
  if (m_loading == loading)
    return;

  m_loading = loading;
  qDebug() << "setLoading(" << loading << ")";
  emit loadingChanged();
}

//------------------------------------------------------------------------------

message_id_t MessageModel::oldestId() const {
  if (m_list.isEmpty())
    return -1;
  return m_list.last()->getId();
}

//------------------------------------------------------------------------------

void MessageModel::clear() {
  beginRemoveRows(QModelIndex(), 0, rowCount()-1);
  m_list.clear();
  endRemoveRows();
}

//------------------------------------------------------------------------------

int MessageModel::insert(Message* msg, int i) {
  if (i == -1)
    i = m_oldestFirst ? 0 : rowCount();

  message_id_t msg_id = msg->getId();

  if (!m_filter.isEmpty()) {
    QString text = msg->getPlainText();
    if ((m_filter.startsWith('/') && m_filter.endsWith('/') &&
         text.contains(QRegExp(m_filter.mid(1,m_filter.length()-2)))) ||
        text.contains(m_filter, Qt::CaseInsensitive)) {
      // filtered++;
      // continue;
      return i;
    }
  }

  if (m_oldestFirst) {
    // Go down the list until we reach the end or a larger message_id
    while (i < rowCount() && m_list[i]->getId() <= msg_id)
      i++;
  } else {
    // Go up the list until we reach the beginning, or a larger message_id
    while (i > 0 && m_list[i-1]->getId() < msg_id)
      i--;
  }
  
  // Skip if message already exists
  if (i > 0 && m_list[i-1]->getId() == msg_id)
    return i;
  
  // Insert message
  beginInsertRows(QModelIndex(), i, i);
  m_list.insert(i, msg); 
  endInsertRows();
  
  return i;
}

//------------------------------------------------------------------------------

bool MessageModel::update(Message* msg) {
  message_id_t msg_id = msg->getId();
  int i=0; 

  while (i<m_list.length()) {
    if (m_list[i]->getId() == msg_id) {
      m_list.replace(i, msg);
      QModelIndex rowi = createIndex(i,0);
      emit dataChanged(rowi, rowi);
      return true;
    }
    i++;
  }

  return false;
}

//------------------------------------------------------------------------------

void MessageModel::insert(MessageList ml) {
  int i = -1; //rowCount(); // Index into m_list, we start at "bottom"

  // Loop over all new messages from ml
  MessageList::list_t::const_iterator mli = ml.constBegin();
  for (; mli != ml.constEnd(); mli++) {
    Message* msg = mli.value();
    i = insert(msg, i);
  }
}

//------------------------------------------------------------------------------

void MessageModel::debugDump() const {
  qDebug() << "MessageModel::debugDump():";
  for (int i=0; i<m_list.count(); i++) {
    Message* msg = qobject_cast<Message*>(m_list[i]);
    if (!msg)
      continue;

    qDebug() << i << "["
             << "id:" << msg->getId()
             // << "conv:" << msg->getConversationPath()
             // << "reply:" << msg->getInReplyToPath()
             << "]:" << msg->getPlainText();
  }
}

//------------------------------------------------------------------------------

int MessageModel::rowCount(const QModelIndex&) const {
  return m_list.count();
}

//------------------------------------------------------------------------------

QString MessageModel::repeatUser(const Message* msg) const {
  const StatusMessage* status = qobject_cast<const StatusMessage*>(msg);
  if (!status || !status->isRepeat())
    return QString();

  User* user = status->getRepeatUser();
  return user->getScreenName();
}

//------------------------------------------------------------------------------

QVariant MessageModel::data(const QModelIndex& index, int role) const {
  if (index.row() < 0 || index.row() >= m_list.count())
    return QVariant();
  
  Message* msg = m_list[index.row()];
  const StatusMessage* status = qobject_cast<const StatusMessage*>(msg);

  User* user = msg->getUser();

  if (role == IdRole)
    return (qlonglong)msg->getId();
  else if (role == UserIdRole)
    return user ? (qlonglong)user->getId() : -1;
  else if (role == UserRole)
    return user ? user->getScreenName() : "";
  else if (role == UserFullRole)
    return user ? user->getFullName() : "";
  else if (role == DateRole)
    return msg->getFuzzyDate();
  else if (role == SourceRole)
    return msg->getSource();
  else if (role == TextRole)
    return msg->getPlainText();
  else if (role == PathRole)
    return msg->getMessagePath();
  else if (role == RepeatRole)
    return repeatUser(msg);
  else if (role == InReplyToRole)
    return status ? status->getInReplyToUser() : "";
  else if (role == ImageFileNameRole)
    return user->getAvatarUrl();
  else if (role == FavouritedRole)
    return status ? status->isFavourited() : false;

  qDebug() << "MessageModel::data(" << index.row() << "," << role << ")"
           << "requested non-existent role.";
  return QVariant();
}

//------------------------------------------------------------------------------

void MessageModel::loadRequest(QSocialAPIRequest* req) {
  connect(req, SIGNAL(timelineReady(MessageList)),
          this, SLOT(onTimelineReady(MessageList)));
  connect(req, SIGNAL(directMessagesReady(MessageList)),
          this, SLOT(onTimelineReady(MessageList)));
  m_sn->executeRequest(req);
  setLoading(true);
}

//------------------------------------------------------------------------------

void MessageModel::onTimelineReady(MessageList list) {
  insert(list);

  // Redraw all messages since relative time stamps may have changed.
  emit dataChanged(createIndex(0,0), createIndex(rowCount()-1, 0));

  setLoading(false);
}
