/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "directmessage.h"
#include "util/util.h"

//------------------------------------------------------------------------------

DirectMessage::DirectMessage(const QDomNode& node, QObject* parent) :
  Message(node,parent) {
  setFromXML(node);
}

//------------------------------------------------------------------------------

void DirectMessage::setFromXML(const QDomNode& node) {
  Message::setFromXML(node);

  plainText = getXMLElementText(node, "text");

  date = getXMLElementText(node, "created_at");

  /*
  <sender_id>777925</sender_id>
  <recipient_id>19345946</recipient_id>
  <sender_screen_name>themattharris</sender_screen_name>
  <recipient_screen_name>mattytest</recipient_screen_name>
  */

  emit hasUpdated();
}

//------------------------------------------------------------------------------
