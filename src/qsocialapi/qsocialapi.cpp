/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "qsocialapi.h"
#include "util/util.h"

//------------------------------------------------------------------------------

void dumpNode(const QDomNode& node) {
  QTextStream ss(stderr);
  node.save(ss,2);
}

//------------------------------------------------------------------------------

QSocialAPI::QSocialAPI(QObject* parent) :
  QObject(parent),
  APIUrl("https://quitter.se/"),
  clientSource("QSocialAPI"),
  ignoreSSLErrors(false),
  config(NULL),
  friendica(false)
{
  net = new QNetworkAccessManager(this);
  cookies = new QNetworkCookieJar(this);
  net->setCookieJar(cookies);

  connect(net, SIGNAL(finished(QNetworkReply*)),
          this, SLOT(onFinished(QNetworkReply*)));
  connect(net, SIGNAL(authenticationRequired(QNetworkReply*, QAuthenticator*)),
          this,
          SLOT(onAuthenticationRequired(QNetworkReply*, QAuthenticator*)));
  connect(net, SIGNAL(sslErrors(QNetworkReply*, const QList<QSslError>&)),
          this, SLOT(onSslErrors(QNetworkReply*, const QList<QSslError>&)));
}


//------------------------------------------------------------------------------

QSocialAPI::~QSocialAPI() {
  if (config)
    delete config;
}

//------------------------------------------------------------------------------

void QSocialAPI::setAPIUrl(QString url) {
  APIUrl = url;
}

void QSocialAPI::setUsername(QString username) {
  this->username = username;
}

void QSocialAPI::setPassword(QString password) {
  this->password = password;
}

void QSocialAPI::setClientSource(QString clientSource) {
  this->clientSource = clientSource;
}

void QSocialAPI::setIgnoreSSLErrors(bool ignoreSSLErrors) {
  this->ignoreSSLErrors = ignoreSSLErrors;
}

//------------------------------------------------------------------------------

void QSocialAPI::onSslErrors(QNetworkReply* reply,
                             const QList<QSslError>& errors) {
  if (ignoreSSLErrors) {
    reply->ignoreSslErrors();
    return;
  }

  for (int i=0; i<errors.size(); i++)
    emit error(errors[i].errorString());
}

//------------------------------------------------------------------------------

void QSocialAPI::onAuthenticationRequired(QNetworkReply* reply, 
                                          QAuthenticator* auth) {
  // Warn if authentication requested of non SSL
  if (APIUrl.startsWith("https") && reply->sslConfiguration().isNull())
    qFatal("No SSL on authentication.");

  auth->setUser(username);
  auth->setPassword(password);
}

//------------------------------------------------------------------------------

QSocialAPIRequest* QSocialAPI::timelineRequest(QString path, message_id_t id,
                                               bool older) {
  QStringList params;
  if (id != -1) 
    params.append(QString("%2=%1").arg(id).arg(older?"max_id":"since_id"));
  params.append(QString("count=%1").arg(20));

  QSocialAPIRequest* rq =
    new QSocialAPIRequest(path, params, false, this);
  return rq;
}

//------------------------------------------------------------------------------

QSocialAPIRequest* QSocialAPI::getServerConfig() {
  return new QSocialAPIRequest("statusnet/config");
}

//------------------------------------------------------------------------------

QSocialAPIRequest* QSocialAPI::postUpdate(QString msg,
                                          message_id_t in_reply_to_id) {
  QStringList params;
  params.append("status="+msg);
  params.append("source="+clientSource);
  if (in_reply_to_id != -1)
    params.append(QString("in_reply_to_status_id=%1").arg(in_reply_to_id));
  
  QSocialAPIRequest* rq =
    new QSocialAPIRequest("statuses/update", params, true, this);
  return rq;
}

//------------------------------------------------------------------------------

QSocialAPIRequest* QSocialAPI::postDirectMessage(QString msg,
                                                 User* recipient) {
  QStringList params;
  params.append("screen_name="+recipient->getScreenName());
  params.append("user_id="+recipient->getId());
  params.append("text="+msg);
  params.append("source="+clientSource);
  
  QSocialAPIRequest* rq =
    new QSocialAPIRequest("direct_messages/new", params, true, this);
  return rq;
}

//------------------------------------------------------------------------------

QSocialAPIRequest* QSocialAPI::getUser(user_id_t id, QString screenName) {
  QStringList params;
  params.append(QString("user_id=%1").arg(id));
  params.append("screen_name="+screenName);

  QSocialAPIRequest* rq =
    new QSocialAPIRequest("users/show", params, true, this);
  return rq;
}

//------------------------------------------------------------------------------

QSocialAPIRequest* QSocialAPI::favourite(bool set, message_id_t id) {
  QString res =
    QString("favorites/%1/%2").arg(set ? "create" : "destroy").arg(id);

  return new QSocialAPIRequest(res, QStringList(), true, this);
}

//------------------------------------------------------------------------------

QSocialAPIRequest* QSocialAPI::friends() {
  return new QSocialAPIRequest("statuses/friends", QStringList(), true, this);
}

//------------------------------------------------------------------------------

QSocialAPIRequest* QSocialAPI::groups() {
  return new QSocialAPIRequest("statusnet/groups/list", QStringList(),
                               true, this);
}

//------------------------------------------------------------------------------

QSocialAPIRequest* QSocialAPI::directMessages() {
  return new QSocialAPIRequest("direct_messages", QStringList(), true, this);
}

//------------------------------------------------------------------------------

QSocialAPIRequest* QSocialAPI::sentDirectMessages() {
  return new QSocialAPIRequest("direct_messages/sent", QStringList(),
                               true, this);
}

//------------------------------------------------------------------------------

QSocialAPIRequest* QSocialAPI::repeatStatus(message_id_t id) {
  QStringList params;
  params.append(QString("id=%1").arg(id));
  return new QSocialAPIRequest(QString("statuses/retweet/%1").arg(id),
                               params, true, this);
}

//------------------------------------------------------------------------------

QSocialAPIRequest* QSocialAPI::verifyCredentials() {
  return new QSocialAPIRequest("account/verify_credentials", QStringList(),
                               false, this);
}

//------------------------------------------------------------------------------

QSocialAPIRequest* QSocialAPI::friendicaPing() {
  QSocialAPIRequest* rq =
    new QSocialAPIRequest("ping", QStringList(), false, this);
  rq->setSilentError();
  return rq;
}

//------------------------------------------------------------------------------

void QSocialAPI::executeRequest(QSocialAPIRequest* rq) {
  if (username.isEmpty())
    return;

  bool do_post = rq->usePost();

  QString url = APIUrl + "api/" + rq->getUrl();

  if (rq->getResource() == "ping")
    url = APIUrl + "ping";

  qDebug() << (do_post ? "[POST]" : "[GET]") << url;
  // qDebug() << rq->getData();

  QNetworkRequest qnr = QNetworkRequest(QUrl(url));

  // if (do_post)
  qnr.setHeader(QNetworkRequest::ContentTypeHeader,
                "application/x-www-form-urlencoded");

  QNetworkReply* reply = do_post ?
    net->post(qnr, rq->getData()) :
    net->get(qnr);

  replyToRequest.insert(reply, rq);
  emit loadingChanged();
}

//------------------------------------------------------------------------------

void QSocialAPI::onFinished(QNetworkReply* reply) {
  bool debug = false;

  QSocialAPIRequest* rq = replyToRequest.take(reply);
  emit loadingChanged();

  bool ping = rq->getResource() == "ping";

  if (reply->error()) {
    QString errorMessage =
      QString("Network error: %1").arg(reply->errorString());
    if (!ping)
      emit error(errorMessage);
    return;
  }

  QDomDocument doc;
  doc.setContent(reply);

  QDomElement e = doc.firstChildElement();
  if (e.isNull())
    qFatal("QSocialAPI::onFinished: No XML element!");
  QString eName = e.tagName();

  // Print xml for debugging
  if (debug)
    dumpNode(doc);
  
  if (eName == "result") {
    parsePing(doc);
    rq->setPing();
  }
  else if (eName == "statuses")
    rq->setTimeline(parseTimeline(doc));
  else if (eName == "status")
    rq->setStatus(parseStatus(e));
  else if (eName == "users")
    rq->setFriends(parseFriends(doc));
  else if (eName == "groups")
    rq->setGroups(parseGroups(doc));
  else if (eName == "direct-messages")
    rq->setDirectMessages(parseDirectMessages(doc));
  else if (eName == "user")
    rq->setUser(parseUser(e));
  else if (eName == "direct_message")
    rq->setDirectMessage(parseDirectMessage(e));
  else if (eName == "config") {
    config = parseConfig(e);
    rq->setConfig(config);
  } else if (eName == "ok") // Friendica does this :)
    rq->setOK();
  else {
    if (!rq->silentError()) {
      dumpNode(doc);
      emit error("Unrecognised response!");
    }
  }

  reply->deleteLater();
}

//------------------------------------------------------------------------------

void QSocialAPI::parsePing(const QDomDocument& /*doc*/) {
  friendica = true;

  // do something fancy with the response, example:
  // <?xml version="1.0" encoding="UTF-8"?>
  //    <result>
  //       <intro>0</intro>
  //       <mail>0</mail>
  //       <net>0</net>
  //       <home>0</home>
  //       <all-events>1</all-events>
  //       <all-events-today>0</all-events-today>
  //       <events>0</events>
  //       <events-today>0</events-today>
  //       <birthdays>1</birthdays>
  //       <birthdays-today>0</birthdays-today>
  //       <notif count="0">
  //       <note href="http://social.feder8.ru/notify/view/5813" name="Gerda de Vos" url="https://sysad.org/u/nypa" photo="http://social.feder8.ru/photo/eebcd93ad331178682062bf42af4848b-5.png" date="16 minutes ago" seen="notify-seen">Gerda de Vos commented on luminocity\'s post</note>
  //       <note href="http://social.feder8.ru/notify/view/5812" name="luminocity" url="https://despora.de/u/luminocity" photo="http://social.feder8.ru/photo/30e886da2c7a0a24d6c496ca5e5c2488-5.png" date="20 minutes ago" seen="notify-seen">luminocity commented on luminocity's post</note>
  //       </notif>
  //       <sysmsgs> </sysmsgs>
  //    </result>
}


QSocialAPIConfig* QSocialAPI::parseConfig(const QDomNode& node) {
  QSocialAPIConfig* config = new QSocialAPIConfig;

  // Store each config value in each config section in the
  // QSocialAPIConfig map as "section/name => "value".

  // There are only ever two levels in the XML so no need for
  // recursion.

  QDomNode n = node.firstChild();
  while (!n.isNull()) {
    if (n.isElement()) {
      QString section = n.nodeName();
      QDomNode cn = n.firstChild();
      while (!cn.isNull()) {
        if (cn.isElement()) {
          QDomElement e = cn.toElement();
          config->insert(section+"/"+e.tagName(), e.text());
          // qDebug() << "config:" << section+"/"+e.tagName()
          //          << "=>" << e.text();
        }
        cn = cn.nextSibling();
      }
    }
    n = n.nextSibling();
  }
  return config;
}

//------------------------------------------------------------------------------

MessageList QSocialAPI::parseTimeline(const QDomDocument& doc) {
  QDomElement sel = doc.firstChildElement("statuses");

  QString elname = "status";
  QDomElement el = sel.firstChildElement(elname);

  MessageList list;

  while (!el.isNull()) {
    StatusMessage* status = parseStatus(el);
    if (status)
      list.append(status);
    el = el.nextSiblingElement(elname);
  }
  return list;
}

//------------------------------------------------------------------------------

UserList QSocialAPI::parseFriends(const QDomDocument& doc) {
  QDomNodeList friendsList = doc.elementsByTagName("user");

  UserList list;
  for (int i=0; i<friendsList.count(); i++) {
    QDomNode node = friendsList.at(i);
    if (getXMLElementText(node, "following") != "true")
      continue;
    list.append(parseUser(node));
  }
  return list;
}

//------------------------------------------------------------------------------

QStringList QSocialAPI::parseGroups(const QDomDocument& doc) {
  QDomNodeList groupList = doc.elementsByTagName("group");

  QStringList list;
  for (int i=0; i<groupList.count(); i++) {
    QDomNode node = groupList.at(i);
    if (getXMLElementText(node, "member") != "true")
      continue;
    // FIXME, should have Group class ...
    list.append(getXMLElementText(node, "nickname"));
  }
  return list;
}


//------------------------------------------------------------------------------

MessageList QSocialAPI::parseDirectMessages(const QDomDocument& doc) {
  QDomNodeList nl = doc.elementsByTagName("direct_message");
  
  MessageList list;
  for (int i=0; i<nl.count(); i++) {
    DirectMessage* dm = parseDirectMessage(nl.at(i));
    list.append(dm);
  }
  return list;
}

//------------------------------------------------------------------------------

User* QSocialAPI::parseUser(const QDomNode& node) {
  int id = getXMLElementText(node, "id").toInt();
  if (id < 1)
    qFatal("parseUser(): id bad");

  if (users.contains(id))
    return users[id];

  User* user = new User(node, this);
  users[id] = user;
  return user;
}
  
//------------------------------------------------------------------------------

StatusMessage* QSocialAPI::parseStatus(const QDomNode& node) {
  QString id_str = getXMLElementText(node, "id");
  int id = id_str.toInt();
  if (id < 1) {
    qDebug() << "WARNING: unable to parse response";
    dumpNode(node);
    return NULL;
  }

  if (statuses.contains(id)) {
    StatusMessage* s = statuses[id];
    s->setFromXML(node);
    return s;
  }

  StatusMessage* status = new StatusMessage(node, this);

  QDomElement el = node.firstChildElement("user");
  if (status->isRepeat()) {
    status->setRepeatUser(parseUser(el));
    QDomElement elRt = node.firstChildElement("retweeted_status");
    el = elRt.firstChildElement("user");
  }

  if (el.isNull()) {
    dumpNode(node);
    qFatal("Bad XML, no user");
  }
  
  status->setUser(parseUser(el));

  statuses[id] = status;
  return status;
}

//------------------------------------------------------------------------------

DirectMessage* QSocialAPI::parseDirectMessage(const QDomNode& node) {
  int id = getXMLElementText(node, "id").toInt();
  if (id < 1)
    qFatal("parseDirectMessage(): bad id");

  if (dms.contains(id)) {
    DirectMessage* dm = dms[id];
    dm->setFromXML(node);
    return dm;
  }

  DirectMessage* dm = new DirectMessage(node, this);

  QDomElement senderElem = node.firstChildElement("sender");
  dm->setUser(parseUser(senderElem));
  
  QDomElement recElem = node.firstChildElement("recipient");
  dm->setRecipientUser(parseUser(recElem));
  
  dms[id] = dm;
  return dm;
}

//------------------------------------------------------------------------------

QString QSocialAPI::getConfig(const QString& key) const {
  if (!config || !config->contains(key))
    return "";
  return config->value(key);
}
  
//------------------------------------------------------------------------------

int QSocialAPI::getTextLimit() const {
  int textlimit = 140;
  QString tlStr = getConfig("site/textlimit");

  if (tlStr == "false" || tlStr.isEmpty()) {
    textlimit = -1;
  } else {
    int tl = tlStr.toInt();
    if (tl > 0)
      textlimit = tl;
  }
  return textlimit;
}

//------------------------------------------------------------------------------

int QSocialAPI::getHttpStatusCode(QSocialAPIRequest* rq) const {
    QString url = APIUrl+"/api/"+rq->getUrl();
    QEventLoop eventLoop;
    QNetworkAccessManager networkAccessManager;
    QObject::connect(&networkAccessManager,SIGNAL(finished(QNetworkReply*)), \
                     &eventLoop,SLOT(quit()));
    QNetworkRequest req = QNetworkRequest(QUrl(url));
    QNetworkReply* reply = networkAccessManager.head(req);
    eventLoop.exec();
    int result = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    delete(reply);
    return result;
}
