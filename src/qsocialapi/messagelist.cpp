/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "messagelist.h"

//------------------------------------------------------------------------------

bool MessageList::contains(message_id_t id) {
  return m_list.contains(id);
}

//------------------------------------------------------------------------------

bool MessageList::contains(Message* msg) {
  return contains(msg->getId());
}
//------------------------------------------------------------------------------

void MessageList::append(Message* msg) {
  message_id_t id = msg->getId();
  m_list.insert(id, msg);

  if (id > m_highestId || m_highestId == -1)
    m_highestId = id;

  if (id < m_lowestId || m_lowestId == -1)
    m_lowestId = id;
}

//------------------------------------------------------------------------------
