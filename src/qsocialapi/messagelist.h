/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MESSAGELIST_H
#define MESSAGELIST_H

#include <QMap>

#include "message.h"
#include "user.h"

class MessageList {
public:
  MessageList() : m_highestId(-1), m_lowestId(-1) {}

  typedef QMap<message_id_t, Message*> list_t;
  
  list_t::const_iterator constBegin() const {
    return m_list.begin();
  }

  list_t::const_iterator constEnd() const {
    return m_list.end();
  }

  message_id_t getHighestId() const { return m_highestId; }

  message_id_t getLowestId() const { return m_lowestId; }

  bool contains(message_id_t);

  bool contains(Message*);

  void append(Message*);

  int count() const { return m_list.count(); }

protected:
  list_t m_list;
  message_id_t m_highestId;
  message_id_t m_lowestId;
};

#endif
