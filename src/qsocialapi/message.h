/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MESSAGE_H
#define MESSAGE_H

#include <QtXml>
#include <QObject>

#include "user.h"

//------------------------------------------------------------------------------

typedef int message_id_t;

//------------------------------------------------------------------------------
  
class Message : public QObject {
  Q_OBJECT
               
public:
  explicit Message(const QDomNode& node, QObject* parent=0);

  explicit Message(message_id_t, const QString&);

  QString getPlainText() const { return plainText; }

  virtual QString getMessagePath() const { return ""; }

  QString getSource() const;

  User* getUser() const { return user; }

  void setUser(User* user) { this->user = user; }

  QString getDate() const { return date; }

  QString getFuzzyDate() const;

  message_id_t getId() const { return id; }

  virtual void setFromXML(const QDomNode& node);

signals:
  void hasUpdated();
  void textHasUpdated();
  
protected:
  static message_id_t getXMLId(const QDomNode& node);

  message_id_t id;

  QString source;
  QString plainText;
  QString date;

  User* user;
};

//------------------------------------------------------------------------------

#endif
