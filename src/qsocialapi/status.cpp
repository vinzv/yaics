/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "status.h"

#include "util/util.h"

#include <QTextDocument>

//------------------------------------------------------------------------------

StatusMessage::StatusMessage(const QDomNode& node, QObject* parent) :
  Message(node, parent),
  m_truncated(false),
  m_isRepeat(false),
  m_hasExpanded(false),
  m_repeatUser(NULL)
{
  setFromXML(node);
}


//------------------------------------------------------------------------------

void StatusMessage::setFromXML(const QDomNode& node) {
  // Id from real status, even if API repeat
  Message::setFromXML(node);

  QDomNode n = node;

  QDomElement el = node.firstChildElement("retweeted_status");
  if (!el.isNull()) {
    m_isRepeat = true;
    n = el;
  } else {
    m_isRepeat = false;
  }

  // Also store "real" id, which for repeats is the id of the repeated
  // notice, otherwise the same as the normal id
  realId = getXMLId(n);

  bool dontTouch = m_hasExpanded && !htmlText.isEmpty();

  if (!dontTouch) {
    htmlText = getXMLElementText(n, "statusnet:html");
    plainText = getXMLElementText(n, "text");
  }

  date = getXMLElementText(n, "created_at");

  source = getXMLElementText(n, "source");

  in_reply_to = getXMLElementText(n, "in_reply_to_status_id");
  in_reply_to_user = getXMLElementText(n, "in_reply_to_screen_name");
  in_reply_to_user_id = getXMLElementText(n, "in_reply_to_user_id");
  
  favourited = (getXMLElementText(n, "favorited") == "true");

  conversation_id = getXMLElementText(n, "statusnet:conversation_id");

  /* This doesn't actually work...
     m_truncated = (getXMLElementText(n, "truncated") == "true"); 
     Let's use an ugly hack instead
  */
  m_truncated =
    htmlText.contains(" class=\"attachment more\" title=\"Show more\"");

  QDomElement attachElem = n.firstChildElement("attachments");
  if (!attachElem.isNull()) {
    QDomNode encNode = attachElem.firstChildElement("enclosure");
    while (!encNode.isNull() && encNode.isElement()) {
      QDomElement encElem = encNode.toElement();

      QString mimeType = encElem.attribute("mimetype");
      QString url = encElem.attribute("url");
      // also size
      if (mimeType.isEmpty() || url.isEmpty()) {
        qDebug() << "[ERROR] unexpected attachment";
        QTextStream ss(stderr);
        encElem.save(ss, 2);
      } else {
        //;
          //qDebug() << "Found attachment with mimetype: " << mimeType;
        if (mimeType == "image/jpeg" or mimeType == "image/png" or mimeType =="image/gif")
        {
            m_attachments.append(StatusAttachment(url, mimeType));
            qDebug() << "Loading image with mimetype: " << mimeType << " from url: " << url;
        }

      }

      encNode = encNode.nextSiblingElement("enclosure");
    }
  }

  /* fields not used yet
  <in_reply_to_user_id>12345</in_reply_to_user_id>

  <geo xmlns:georss="http://www.georss.org/georss">
    <georss:point>60.2052 24.6522</georss:point>
  </geo>

  <statusnet:conversation_id>1</statusnet:conversation_id>

  */

  emit hasUpdated();
}
//------------------------------------------------------------------------------
QString StatusMessage::getAttachedImageURL() const {
  if (m_attachments.isEmpty())
  return "";

  return m_attachments.at(0).url();
}
//------------------------------------------------------------------------------

QString StatusMessage::getConversationPath() const {
  if (conversation_id.isEmpty())
    return "";

  return "conversation/"+conversation_id+
    (in_reply_to.isEmpty()?"":"#notice-"+in_reply_to);
}

//------------------------------------------------------------------------------

int StatusMessage::getConversationId() const {
  bool ok;
  if (conversation_id.isEmpty())
    return -1;

  int ret = conversation_id.toInt(&ok, 10);

  return ok ? ret : -1;
}

//------------------------------------------------------------------------------

QString StatusMessage::getInReplyToPath() const {
  if (in_reply_to.isEmpty())
    return "";
  return "notice/"+in_reply_to;
}

//------------------------------------------------------------------------------

bool StatusMessage::expandable() const {
  return m_truncated && m_attachments.size();
}

//------------------------------------------------------------------------------

void StatusMessage::updateFromAttachment(const QString& fileName) {
  QFile fp(fileName);
  if (!fp.open(QIODevice::ReadOnly | QIODevice::Text))
    return;

  QTextStream in(&fp);
  QString origHtml = in.readAll();

  QRegExp rx("<body>(.*)</body>");
  if (rx.indexIn(origHtml) != -1) {
    htmlText = rx.cap(1);
    
    QTextDocument doc;
    doc.setHtml(origHtml);
    plainText = doc.toPlainText();
    m_hasExpanded = true;
    qDebug() << "[DEBUG] Expanded message" << getMessagePath();
  }
  emit textHasUpdated();
}

//------------------------------------------------------------------------------

