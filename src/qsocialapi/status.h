/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STATUS_H
#define STATUS_H

#include <QtXml>
#include <QObject>

#include "user.h"
#include "message.h"

//------------------------------------------------------------------------------

class StatusAttachment {
public:
  explicit StatusAttachment(QString url, QString mimeType) :
    m_url(url), m_mimeType(mimeType) {}

  QString url() const { return m_url; }
  QString mimeType() const { return m_mimeType; }
  
private:
  QString m_url;
  QString m_mimeType;
};

//------------------------------------------------------------------------------

class StatusMessage : public Message {
  Q_OBJECT

public:
  explicit StatusMessage(const QDomNode& node, QObject* parent=0);

  QString getHTML() const { return htmlText; }

  virtual QString getMessagePath() const {
    return QString("notice/%1").arg(id);
  }

  User* getRepeatUser() const { return m_repeatUser; }

  void setRepeatUser(User* user) { this->m_repeatUser = user; }

  QString getInReplyToPath() const;

  QString getInReplyToUser() const { return in_reply_to_user; }

  QString getConversationPath() const;
  QString getAttachedImageURL() const;

  int getConversationId() const;

  message_id_t getRealId() const { return realId; }

  bool isFavourited() const { return favourited; }

  bool isRepeat() const { return m_isRepeat; }

  virtual void setFromXML(const QDomNode& node);

  bool expandable() const;
  
  QList<StatusAttachment> attachments() const { return m_attachments; }

public slots:
  void updateFromAttachment(const QString&);

protected:
  message_id_t realId;

  QString htmlText;

  // QString source;
  QString in_reply_to;
  QString in_reply_to_user;
  QString in_reply_to_user_id;
  QString conversation_id;

  bool m_truncated;
  QList<StatusAttachment> m_attachments;

  bool favourited;
  bool m_isRepeat;
  
  bool m_hasExpanded;
  
  User* m_repeatUser;
};

#endif
