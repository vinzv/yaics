/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef USER_H
#define USER_H

#include <QObject>
#include <QtXml>

//------------------------------------------------------------------------------

typedef signed long int user_id_t;

//------------------------------------------------------------------------------

class User : public QObject {
  Q_OBJECT

public:
  User(const QDomNode& node, QObject* parent=0);

  QString getFullName() const { return fullName; }
  QString getScreenName() const { return screenName; }
  QString getUrl() const { return url; }
  QString getDomain() const { return urlDomain; }
  QString getAvatarUrl() const { return avatarUrl; }

  user_id_t getId() const { return id; }

  void update(const QDomNode& node);

signals:
  void avatarChanged();

private:
  void setFromXML(const QDomNode& node);

  QString fullName;
  QString screenName;
  user_id_t id;
  QString url;
  QString urlDomain;

  QString avatarUrl;
  
  QString avatarFileName;
};

typedef QList<User*> UserList;

#endif
