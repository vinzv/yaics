/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _QSOCIALAPIREQUEST_H_
#define _QSOCIALAPIREQUEST_H_

#include <QObject>

#include "status.h"
#include "messagelist.h"
#include "user.h"
#include "directmessage.h"

typedef QMap<QString, QString> QSocialAPIConfig;
typedef QMap<QString, QString>::const_iterator QSocialAPIConfig_citer;

class QSocialAPIRequest : public QObject {
  Q_OBJECT

public:
  QSocialAPIRequest(QObject* parent=0);

  QSocialAPIRequest(QString resource, QStringList params = QStringList(),
                    bool doPost=false, QObject* parent=0);

  QString getResource() const { return origResource; }

  QByteArray getUrl() const { return requestPath; }

  QByteArray getData() const { return data; }

  void setPing();

  void setOK();

  void setTimeline(MessageList);

  void setStatus(StatusMessage*);

  void setFriends(UserList);

  void setGroups(QStringList);

  void setDirectMessages(MessageList);

  void setUser(User*);

  void setDirectMessage(DirectMessage*);

  void setConfig(QSocialAPIConfig*);

  bool usePost() const { return post; }

  void setSilentError() { mSilentError = true; }
  bool silentError() const { return mSilentError; }
  
signals:
  void pingReady();
  void OKReady();
  void timelineReady(MessageList);
  void statusReady(StatusMessage*);
  void friendsReady(UserList);
  void groupsReady(QStringList);
  void directMessagesReady(MessageList);
  void userReady(User*);
  void directMessageReady(DirectMessage*);
  void configReady(QSocialAPIConfig*);

protected:
  QByteArray requestPath;
  QByteArray data;
  bool post;
  QString origResource;
  bool mSilentError;
};

#endif
