/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DIRECTMESSAGE_H
#define DIRECTMESSAGE_H

#include <QtXml>
#include <QObject>

#include "user.h"
#include "message.h"
//------------------------------------------------------------------------------

class DirectMessage : public Message {
  Q_OBJECT

public:
  DirectMessage(const QDomNode& node, QObject* parent=0);

  virtual QString getMessagePath() const {
    return QString("message/%1").arg(id);
  }

  User* getRecipientUser() const { return recipientUser; }

  void setRecipientUser(User* user) { this->recipientUser = user; }

  virtual void setFromXML(const QDomNode& node);

protected:
  User* recipientUser;
};

//------------------------------------------------------------------------------

#endif
