/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "util/filedownloader.h"

#include "avatar.h"
#include "qgraphicseffect.h"

//------------------------------------------------------------------------------

Avatar::Avatar(User* user, QWidget* parent) : QLabel(parent) {
  this->user = user;
  url = user->getAvatarUrl();
  setScaledContents(true);
  QGraphicsDropShadowEffect* dropShadowEffect = new QGraphicsDropShadowEffect(this);
  setGraphicsEffect(dropShadowEffect);
  setMaximumSize(48,48);
  setFocusPolicy(Qt::NoFocus);
  updatePixmap();
  connect(user, SIGNAL(avatarChanged()), this, SLOT(onAvatarChanged()));
}

//------------------------------------------------------------------------------

void Avatar::onAvatarChanged() {
  url = user->getAvatarUrl();
  updatePixmap();
}

//------------------------------------------------------------------------------

void Avatar::fileReady(const QString& fn) {
  updatePixmap(fn);
}

//------------------------------------------------------------------------------

void Avatar::updatePixmap(const QString& fileName) {
  static QString defaultImage = ":/images/default.png";
  QString fn = fileName;

  // if (fn.isEmpty()) {
  //   FileDownloader* fd = new FileDownloader(this);
  //   connect(fd, SIGNAL(fileReady(const QString&)),
  //           this, SLOT(fileReady(const QString&)));
  //   fn = fd->getFile(url);
  // }

  // if (fn.isEmpty()) {
  //   FileDownloader* fd = NULL;
  //   fn = FileDownloader::getFile(url, fd);
  //   if (fd)
  //     connect(fd, SIGNAL(fileReady(const QString&)),
  //             this, SLOT(fileReady(const QString&)));
  // }

  if (fn.isEmpty()) {
    FileDownloader* fd = FileDownloader::get(url);

    if (fd->ready()) {
      fn = fd->fileName();
      fd->deleteLater();
    } else {
      connect(fd, SIGNAL(fileReady(const QString&)),
              this, SLOT(fileReady(const QString&)));
      fd->download();
    }
  }


  if (fn.isEmpty())
    fn = defaultImage;
  if (fn != localFile) {
    localFile = fn;
    QPixmap pix(localFile);
    if (pix.isNull())
      pix.load(localFile,"JPEG");
    if (pix.isNull())
      pix.load(localFile,"PNG");
    if (pix.isNull()) {
      localFile = defaultImage;
      pix.load(localFile);
    }
    setPixmap(pix);
  }
}    
