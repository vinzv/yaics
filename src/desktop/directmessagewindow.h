/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DIRECT_MESSAGE_WINDOW_H
#define DIRECT_MESSAGE_WINDOW_H

#include <QWidget>
#include <QComboBox>

#include "qsocialapi/directmessage.h"
#include "qsocialapi/user.h"

#include "messagewindow.h"

//------------------------------------------------------------------------------

class DirectMessageWindow : public MessageWindow {
  Q_OBJECT
public:
  DirectMessageWindow(QSocialAPI* sa, const QString& msg, user_id_t id,
                      QWidget* parent=0);

  QSocialAPIRequest* sendMessageRequest(const QString& msg);

private slots:
  void onDirectMessageReady(DirectMessage*);

private:
  user_id_t recipient_id;
  User* recipient;
  QComboBox* recipientBox;
};

#endif
