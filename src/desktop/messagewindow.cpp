/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "util/util.h"
#include "util/urlshortener.h"
#include "util/yaicssettings.h"

#include "messagewindow.h"

//------------------------------------------------------------------------------

MessageWindow::MessageWindow(QSocialAPI* sa, const QString& reply_to_user,
                             QWidget* parent) :
  QDialog(parent), socialAPI(sa) 
{
  // setWindowFlags(Qt::Dialog);
  // setWindowModality(Qt::NonModal);
  infoLabel = new QLabel(this);
  infoLayout = new QHBoxLayout;
  infoLayout->addWidget(infoLabel);

  textEdit = new MessageEdit(this);

  YaicsSettings* settings = YaicsSettings::getSettings();
  if (settings->getPrependReplies() && !reply_to_user.isEmpty())
      textEdit->setPlainText(reply_to_user+" ");

  countLabel = new QLabel("0", this);
  connect(textEdit, SIGNAL(textChanged()), this, SLOT(updateCount()));
  connect(textEdit, SIGNAL(ready()), this, SLOT(accept()));

  layout = new QVBoxLayout;
  layout->addLayout(infoLayout);
  layout->addWidget(textEdit);
  layout->addWidget(countLabel);

  cancelButton = new QPushButton(tr("Cancel"));
  connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
  
  sendButton = new QPushButton(tr("Send message"));
  connect(sendButton, SIGNAL(clicked()), this, SLOT(accept()));
  sendButton->setDefault(true);

  buttonLayout = new QHBoxLayout;
  buttonLayout->addWidget(cancelButton);
  buttonLayout->addWidget(sendButton);
  layout->addLayout(buttonLayout);
  
  setLayout(layout);

  textEdit->setFocus(Qt::OtherFocusReason);

  QTextCursor cursor = textEdit->textCursor();
  cursor.movePosition(QTextCursor::End);
  textEdit->setTextCursor(cursor);

  updateCount();
}

//------------------------------------------------------------------------------

void MessageWindow::showEvent(QShowEvent*) {
  textEdit->setFocus(Qt::OtherFocusReason);
}

//------------------------------------------------------------------------------

void MessageWindow::accept() {
  if (!socialAPI)
    qFatal("MessageWindow without SocialAPI instance.");

  int textlimit = socialAPI->getTextLimit();
  
  int count = textEdit->toPlainText().count();
  if (textlimit > 0 && count > textlimit)
    return;

  QString msg = textEdit->toPlainText();

  QSocialAPIRequest* rq = sendMessageRequest(msg);
  
  if (!rq)
    return;

  countLabel->setText(tr("Sending ..."));

  socialAPI->executeRequest(rq);
}

//------------------------------------------------------------------------------

void MessageWindow::updateCount() {
  int count = textEdit->toPlainText().count();
  QString countStr = QString::number(count);
  int textlimit = socialAPI->getTextLimit();
  if (textlimit > 0 && count > textlimit) {
    countStr = "<font color=\"red\"><b>" + QString::number(textlimit - count) +
      "</b></font>";
    sendButton->setEnabled(false);
    //checkForUrlsToShorten();
  } else {
    sendButton->setEnabled(true);
  }
  countLabel->setText(countStr);
}

//------------------------------------------------------------------------------

void MessageWindow::checkForUrlsToShorten() {
  static QString oldText;
  QString text = textEdit->toPlainText();
  if (oldText == text)
    return;
  oldText = text;

  QRegExp rx(URL_REGEX);

  int pos = 0;
  while ((pos = rx.indexIn(text, pos)) != -1) {
    int len = rx.matchedLength();

    QString urlStr = rx.cap();

    UrlShortener* us = new UrlShortener(urlStr, this);
    connect(us, SIGNAL(urlReady(const QString&, const QString&)),
            this, SLOT(onUrlReady(const QString&, const QString&)));
    QString newUrlStr = us->shortUrl();
    if (urlStr != newUrlStr)
      text.replace(pos, len, newUrlStr);

    pos += newUrlStr.count();
  }
  textEdit->clear();
  textEdit->insertPlainText(text);
}

//------------------------------------------------------------------------------

void MessageWindow::onUrlReady(const QString&, const QString&) {
  checkForUrlsToShorten();
}
