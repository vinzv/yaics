/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "util/util.h"
#include "util/shorturlexpander.h"
#include "util/filedownloader.h"
#include "yaicsapp.h"
#include "messagewidget.h"
#include "timeline.h"
#include "yaicstabwidget.h"
#include <QWebView>
#include <QUrl>

//------------------------------------------------------------------------------

MessageWidget::MessageWidget(Message* m, QSocialAPI* sa, bool dmButton,
                             QWidget* parent) :
  QFrame(parent), 
  useDMButton(dmButton), 
  msg(m), 
  socialAPI(sa)
{
 QWebView* m_pWebView = NULL;
  if (!m)
    qFatal("MessageWidget(): m == NULL!");

  status = qobject_cast<StatusMessage*>(msg);
  dm = qobject_cast<DirectMessage*>(msg);

  if (status && dm)
    qFatal("MessageWidget(): both status and dm are != NULL!");

  textLabel = new StatusLabel(this);
  connect(textLabel, SIGNAL(linkHovered(const QString&)),
          this,  SIGNAL(linkHovered(const QString&)));


  updateTime(false);
  updateText(true);

  QString myScreenName = socialAPI->getUsername();
  User* user = m->getUser();
  if (myScreenName == user->getScreenName())
    useDMButton = false;

  if (status) {
    favourButton = new QToolButton();
    favourButton->setToolButtonStyle(Qt::ToolButtonTextOnly);
    favourButton->setToolTip(tr("Mark notice as favourite"));
    favourButton->setFocusPolicy(Qt::NoFocus);
    updateFavourButton();
    connect(favourButton, SIGNAL(clicked()), this, SLOT(favourite()));

    repeatButton = new QToolButton();
    repeatButton->setText(QChar(0x267A));
    repeatButton->setToolButtonStyle(Qt::ToolButtonTextOnly);
    repeatButton->setToolTip(tr("Repeat notice"));
    repeatButton->setFocusPolicy(Qt::NoFocus);
    connect(repeatButton, SIGNAL(clicked()), this, SLOT(repeat()));

    replyButton = new QToolButton();
    replyButton->setText(tr("reply"));
    replyButton->setToolButtonStyle(Qt::ToolButtonTextOnly);
    replyButton->setToolTip(tr("Reply to notice"));
    replyButton->setFocusPolicy(Qt::NoFocus);
    connect(replyButton, SIGNAL(clicked()), this, SLOT(reply()));

    conversationButton = new QToolButton();
    conversationButton->setText(tr("conversation"));
    conversationButton->setToolButtonStyle(Qt::ToolButtonTextOnly);
    conversationButton->setToolTip(tr("Open conversation tab"));
    conversationButton->setFocusPolicy(Qt::NoFocus);
    //conversationButton->hide();
    connect(conversationButton,SIGNAL(clicked()), this, SLOT(conversation()));
  }

YaicsSettings* settings;
settings = YaicsSettings::getSettings(this);

if (settings->getshowAttachment())
{
  if (status->getAttachedImageURL() != "")
  {
   //Create widget for viewing image, send in the url

      m_pWebView = new QWebView(this);
         //set position and size
         m_pWebView->setGeometry(0,0,10,10);
         QString tmpurl = status->getAttachedImageURL();

         if (tmpurl != "")
         {
             qDebug() << "Trying to load attached image: "  << tmpurl;
             m_pWebView->load(QUrl(tmpurl));
         }
  }
}

  if (useDMButton) {
    directButton = new QToolButton();
    directButton->setText(tr("direct"));
    directButton->setToolButtonStyle(Qt::ToolButtonTextOnly);
    directButton->setToolTip(tr("Send direct message"));
    directButton->setFocusPolicy(Qt::NoFocus);
    connect(directButton, SIGNAL(clicked()), this, SLOT(direct()));
  }

  buttonLayout = new QHBoxLayout;

  buttonLayout->addStretch();
  if (status) {
    buttonLayout->addWidget(favourButton, 0, Qt::AlignTop);
    buttonLayout->addWidget(repeatButton, 0, Qt::AlignTop);
  }

  if (useDMButton)
    buttonLayout->addWidget(directButton, 0, Qt::AlignTop);

  if (status) {
    buttonLayout->addWidget(replyButton, 0, Qt::AlignTop);
    buttonLayout->addWidget(conversationButton,0,Qt::AlignTop);
  }

  statusLayout = new QVBoxLayout;
  statusLayout->addWidget(textLabel);
  statusLayout->addLayout(buttonLayout);

  if (m_pWebView != NULL)
  {
    statusLayout->addWidget(m_pWebView);
  }
  statusLayout->setContentsMargins(0, 0, 0, 0);
  setLayout(statusLayout);

  connect(msg, SIGNAL(hasUpdated()), this, SLOT(onMessageHasUpdated()));
  connect(msg, SIGNAL(textHasUpdated()), this, SLOT(onTextHasUpdated()));

  if (status && status->expandable()) {
    QList<StatusAttachment> al = status->attachments();
    QString url;
    for (int i=0; i<al.size() && url.isEmpty(); i++)
      if (al[i].mimeType() == "text/html" && !al[i].url().isEmpty())
        url = al[i].url();
    
    if (!url.isEmpty()) {
      FileDownloader* fd = FileDownloader::get(url);

      if (fd->ready()) {
        status->updateFromAttachment(fd->fileName());
        fd->deleteLater();
      } else {
        connect(fd, SIGNAL(fileReady(const QString&)),
                status, SLOT(updateFromAttachment(const QString&)));
        fd->download();
      }
    }
  }
}

//------------------------------------------------------------------------------

void MessageWidget::mousePressEvent(QMouseEvent* e) {
  emit clickedStatus(msg->getId());
  QFrame::mousePressEvent(e);
}

//------------------------------------------------------------------------------

void MessageWidget::updateFavourButton(bool wait) {
  if (!status)
    return;

  QString text = status->isFavourited() ? QChar(0x2605) : QChar(0x2606);
  if (wait)
    text = "...";
  favourButton->setText(text);
}

//------------------------------------------------------------------------------

void MessageWidget::updateText(bool generateText) {
  QString screenName = getUser()->getScreenName();
  QString userUrl = getUser()->getUrl();
  if (screenName.isEmpty())
    screenName = "[" + userUrl + "]";

  // If we use dark style...
  QString newText = "<p><b><a href=\"" + userUrl + "\">" + screenName +
    "</a></b>" + infoString + "</p><p>";

  if (generateText)
    processText();

  newText += text+"</p>";
  textLabel->setText(newText);
}

//------------------------------------------------------------------------------

void MessageWidget::updateTime(bool updateWidget) {
  bool friendica = socialAPI->isFriendica();

  QString dateStr = fuzzyDate(msg->getDate());

  QString apiUrl = slashify(socialAPI->getAPIUrl());
  QString convPath = status ? status->getConversationPath() : "";
  QString reply_user = status ? status->getInReplyToUser() : "";

  infoString = "";

  if (status && !convPath.isEmpty() && !reply_user.isEmpty())
    infoString += QString(" ")+QChar(0x25B8) + " " + reply_user;
  infoString += ", ";

  infoString += " "+ahref(apiUrl+msg->getMessagePath(), dateStr);

  const QString& source = msg->getSource();
  if (!source.isEmpty())
    infoString += tr(" from ")+source;

  if (status) {
    if (!friendica && !convPath.isEmpty())
      infoString += " "+ahref(apiUrl+convPath, tr("in context"));
    else if (!reply_user.isEmpty())
      infoString += " "+ahref(apiUrl+status->getInReplyToPath(),
                              tr("in reply to ")+reply_user);

    if (status->isRepeat()) {
      User* u = status->getRepeatUser();
      QString userStr = u ? ahref(u->getUrl(), u->getScreenName()) : tr("unknown");
      infoString += " "+QString(tr("[repeated by %1]")).arg(userStr);
    }
  }

  if (updateWidget)
    updateText(false);
}

//------------------------------------------------------------------------------

void MessageWidget::favourite() {
  if (!status)
    return;

  updateFavourButton(true);
  QSocialAPIRequest* rq =
    socialAPI->favourite(!status->isFavourited(), status->getRealId());
  connect(rq, SIGNAL(statusReady(StatusMessage*)),
          this, SLOT(onStatusReady(StatusMessage*)));
  socialAPI->executeRequest(rq);
}

//------------------------------------------------------------------------------

void MessageWidget::onStatusReady(StatusMessage*) {
  updateFavourButton();
  updateText(false);
}

//------------------------------------------------------------------------------

void MessageWidget::onMessageHasUpdated() {
  onStatusReady(NULL);
}

//------------------------------------------------------------------------------

void MessageWidget::onTextHasUpdated() {
  updateText(true);
}

//------------------------------------------------------------------------------

void MessageWidget::repeat() {
  YaicsSettings* settings = YaicsSettings::getSettings();

  if (settings->getUseApiRepeat()) {
    QSocialAPIRequest* rq =
      socialAPI->repeatStatus(status->getRealId());
    connect(rq, SIGNAL(statusReady(StatusMessage*)),
            this, SIGNAL(requestReload()));
    connect(rq, SIGNAL(OKReady()), this, SIGNAL(requestReload()));
    socialAPI->executeRequest(rq);
  } else {
    QString rtext = getPlainText();

    QRegExp rxx(GROUP_USER_TAG_REGEXP);
  
    int pos = 0;
    while ((pos = rxx.indexIn(rtext, pos)) != -1) {
      int len = rxx.matchedLength();
      if (rxx.cap(2) == "!")
        rtext.replace(pos, len, rxx.cap(1) + "#" + rxx.cap(3));
      pos += len;
    }

    QString msg = QChar(0x267A)+QString(" @%1: %2").
      arg(getUser()->getScreenName()).arg(rtext);
    emit replySignal(msg);
  }
}

//------------------------------------------------------------------------------

void MessageWidget::reply() {
  if (status) {
    emit replySignal("@"+getUser()->getScreenName(), status->getRealId());
  }
}

//------------------------------------------------------------------------------

void MessageWidget::direct() {
  emit directMessageSignal(getUser()->getId());
}

//------------------------------------------------------------------------------

void MessageWidget::conversation()
{
    if (status != NULL)
    {
        qDebug() << "Conversation button pressed. It's id is:" << status->getConversationId();
        int convId = status->getConversationId();

        if (convId != -1)
        {
            emit showConversation(convId);
        }else
        {
            qDebug() << "Error fetching conversation ID =" << convId;
        }
    }
}

void MessageWidget::processText() {
  bool do_expansions = true;
  QString search_prefix = "";

  if (status) {
    text = status->getHTML();
    do_expansions = false;
    search_prefix = "href=\"";
  } else {
    text = msg->getPlainText();
    text.replace("<", "&lt;");
    text.replace(">", "&gt;");
  }

  // Shorten links that are too long, this is OK, since you can still
  // click the link.
  QRegExp rxa("<a\\s[^>]*href=\"?([^>\"]+)[^>]*>([^<]*)</a>");

  QRegExp rxShortUrls(shortUrls);

  int pos = 0;
  while ((pos = rxa.indexIn(text, pos)) != -1) {
    int len = rxa.matchedLength();
    QString url = rxa.cap(1).trimmed();
    QString linkText = rxa.cap(2);

    bool changeLink = false;

    if (url.contains(rxShortUrls)) {
      // qDebug() << "[LONGURL] Trying" << url;
      ShortUrlExpander* sue = new ShortUrlExpander(url, this);
      connect(sue, SIGNAL(urlReady(const QString&, const QString&)),
	      this, SLOT(onUrlReady(const QString&, const QString&)));
      
      const QString longUrl = sue->longUrl();
      if (url != longUrl) {
	qDebug() << "[LONGURL]" << url << "=>" << longUrl;
	url = longUrl;
	changeLink = true;
      }
    }

    if ((linkText.startsWith("http://") || linkText.startsWith("https://")) &&
	linkText.length() > MAX_WORD_LENGTH) {
	linkText = linkText.left(MAX_WORD_LENGTH-3) + "...";
	changeLink = true;
    }

    if (changeLink) {
      QString newText = QString("<a href=\"%1\">%2</a>").arg(url).arg(linkText);
      text.replace(pos, len, newText);
      pos += newText.length();
    } else {
      pos += len;
    }
  }

  if (!do_expansions)
    return;

  QRegExp rxx(GROUP_USER_TAG_REGEXP);
  
  pos = 0;
  while ((pos = rxx.indexIn(text, pos)) != -1) {
    int len = rxx.matchedLength();

    QString mode = rxx.cap(2);
    QString name = rxx.cap(3);
    
    QString mpath;
    if (mode == "!") mpath = "group/";
    else if (mode == "#") mpath = "tag/";
    else if (mode == "@") mpath = "";

    QString newName = name.size()>MAX_WORD_LENGTH ?
      name.mid(0, MAX_WORD_LENGTH)+"..." : name;

    // QString apiUrl = slashify(socialAPI->getAPIUrl());
    // QString newText = rxx.cap(1) + mode +
    //   ahref(apiUrl+mpath+name, newName);

    QString newText = rxx.cap(1) + mode + wrapTag("b", newName);

    text.replace(pos, len, newText);
    pos += newText.count();
  }

  QRegExp rxxx("(^|\\s)([^\\s<>\"]{40,})(\\s|$)");
  pos = 0;
  while ((pos = rxxx.indexIn(text, pos)) != -1) {
    int len = rxxx.matchedLength();
    QString word = rxxx.cap(2);
    QString newText = rxxx.cap(1);

    int wpos = 0;
    while (true) {
      newText += word.mid(wpos, 5);
      wpos += 5;
      if (wpos >= word.length())
        break;
      else
        newText += "&shy;";
    }
    //    qDebug() << "REPLACED:" << word << "=>" << newText;

    text.replace(pos, len, newText);
    pos += newText.count();
  }
}

//------------------------------------------------------------------------------

void MessageWidget::onUrlReady(const QString&, const QString&) {
  updateText(true);
}
