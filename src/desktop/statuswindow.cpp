/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "statuswindow.h"

//------------------------------------------------------------------------------

StatusWindow::StatusWindow(QSocialAPI* sa, const QString& reply_to_user,
                           message_id_t id, QWidget* parent) :
  MessageWindow(sa, reply_to_user, parent), in_reply_to_id(id) 
{
  sendButton->setText(tr("Send notice"));
  if (in_reply_to_id == -1)
    infoLabel->setText(tr("Write a new notice"));
  else
    infoLabel->setText(tr("Write a reply to %1").arg(reply_to_user));
}

//------------------------------------------------------------------------------

void StatusWindow::onStatusReady(StatusMessage*) {
  done(QDialog::Accepted);
  emit messageSent();
}

//------------------------------------------------------------------------------

QSocialAPIRequest* StatusWindow::sendMessageRequest(const QString& msg) {
  QSocialAPIRequest* rq = socialAPI->postUpdate(msg, in_reply_to_id);
  connect(rq, SIGNAL(statusReady(StatusMessage*)),
          this, SLOT(onStatusReady(StatusMessage*)));
  return rq;
}
