/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MESSAGE_WINDOW_H
#define MESSAGE_WINDOW_H

#include <QDialog>
#include <QShowEvent>
#include <QVBoxLayout>
#include <QLabel>
#include <QHBoxLayout>
#include <QPushButton>

#include "qsocialapi/qsocialapi.h"

#include "messageedit.h"

//------------------------------------------------------------------------------

class MessageWindow : public QDialog {
  Q_OBJECT
public:
  MessageWindow(QSocialAPI* sa, const QString& reply_to_user, QWidget* parent=0);

  virtual QSocialAPIRequest* sendMessageRequest(const QString& msg) = 0;

  virtual void accept();

protected:
  void showEvent(QShowEvent*);
  void checkForUrlsToShorten();

signals:
  void messageSent();

private slots:
  void updateCount();
  void onUrlReady(const QString&, const QString&);

protected:
  QVBoxLayout* layout;

  QLabel* infoLabel;
  QHBoxLayout* infoLayout;

  QLabel* countLabel;

  MessageEdit* textEdit;
  QHBoxLayout* buttonLayout;

  QPushButton* cancelButton;
  QPushButton* sendButton;

  QSocialAPI* socialAPI;
};

#endif
