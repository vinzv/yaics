/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MESSAGEWIDGET_H
#define MESSAGEWIDGET_H

#include <QtCore>
#include <QtXml>

#include <QToolButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFrame>
#include <QWidget>
#include <QMouseEvent>

#include "qsocialapi/user.h"
#include "qsocialapi/message.h"
#include "qsocialapi/status.h"
#include "qsocialapi/directmessage.h"
#include "qsocialapi/qsocialapi.h"

#include "statuslabel.h"

#define MAX_WORD_LENGTH       40
#define GROUP_USER_TAG_REGEXP "(^|\\s)([!@#])([-\\w]+)"

class MessageWidget : public QFrame {
  Q_OBJECT

public:
  MessageWidget(Message* m, QSocialAPI* sa, bool dmButton=true,
                QWidget* parent=0);

  QString getPlainText() const { return msg->getPlainText(); }

  User* getUser() const { return msg->getUser();  }

  void updateTime(bool updateWidget=true);

  message_id_t getId() const { return msg->getId(); }

protected:
  void mousePressEvent(QMouseEvent* e);

signals:
  void replySignal(const QString&, message_id_t=-1);
  void directMessageSignal(user_id_t user_id);
  void linkHovered(const QString&);

  void clickedStatus(message_id_t);
  void requestReload();

  void showConversation(int);
  
public slots:
  void favourite();
  void repeat();
  void reply();
  void direct();              
  void conversation();

private slots:
  void onStatusReady(StatusMessage*);
  void onMessageHasUpdated();
  void onTextHasUpdated();
  void onUrlReady(const QString&, const QString&);

private:
  void setStyleSheet(); 

  void updateFavourButton(bool wait = false);
  void updateText(bool generateText = false);
  void processText();

  QString text;

  QString infoString;

  bool useDMButton;
  
  StatusLabel* textLabel;

  QToolButton* favourButton;
  QToolButton* repeatButton;
  QToolButton* replyButton;
  QToolButton* directButton;
  QToolButton* conversationButton;

  QHBoxLayout* buttonLayout;
  QVBoxLayout* statusLayout;
  
  Message* msg;
  StatusMessage* status;
  DirectMessage* dm;

  QSocialAPI* socialAPI;
};

#endif
