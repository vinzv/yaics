/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "directmessagewindow.h"

//------------------------------------------------------------------------------

DirectMessageWindow::DirectMessageWindow(QSocialAPI* os, const QString& msg,
                                         user_id_t id, QWidget* parent) :
  MessageWindow(os, msg, parent), recipient_id(id), recipient(NULL)
{
  infoLabel->setText(tr("Write a direct message to "));

  recipientBox = new QComboBox(this);

  QString myScreenName = socialAPI->getUsername();

  QList<User*> users = socialAPI->getCurrentUsers();
  QList<User*>::const_iterator it = users.constBegin();
  for (; it != users.constEnd(); it++) {
    User* u = *it;
    user_id_t uid = u->getId();
    QString screenName = u->getScreenName();
    if (screenName == myScreenName)
      continue;

    recipientBox->addItem(screenName, (qlonglong)uid);
    if (uid == id)
      recipientBox->setCurrentIndex(recipientBox->count()-1);
  }

  infoLayout->addWidget(recipientBox);

  sendButton->setText(tr("Send direct message"));
}

//------------------------------------------------------------------------------

void DirectMessageWindow::onDirectMessageReady(DirectMessage*) {
  done(QDialog::Accepted);
  emit messageSent();
}

//------------------------------------------------------------------------------

QSocialAPIRequest* DirectMessageWindow::sendMessageRequest(const QString& msg) {
  int ci = recipientBox->currentIndex();
  user_id_t uid = recipientBox->itemData(ci).toLongLong();

  recipient = socialAPI->getCurrentUser(uid);
  if (!recipient)
    qFatal("Couldn't find selected user in list.");

  QSocialAPIRequest* rq = socialAPI->postDirectMessage(msg, recipient);
  connect(rq, SIGNAL(directMessageReady(DirectMessage*)),
          this, SLOT(onDirectMessageReady(DirectMessage*)));
  return rq;
}
