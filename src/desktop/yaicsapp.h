/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef YAICSAPP_H
#define YAICSAPP_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QMessageBox>

#ifdef USE_DBUS
#include <QDBusInterface>
#endif

#include "util/yaicssettings.h"
#include "qsocialapi/qsocialapi.h"

#include "timeline.h"
#include "yaicstabwidget.h"
#include "yaicssettingsdialog.h"
#include "util/yaics_defines.h"

class YaicsApp : public QMainWindow {
  Q_OBJECT

public:
  YaicsApp();
  ~YaicsApp();

  static QStringList getGroups();
  static QStringList getFriends();
  TimeLine* conversationTimeLine;
protected:
  void timerEvent(QTimerEvent*);
  void showEvent(QShowEvent*);

public slots:
  void statusMessage(const QString& msg);
  void notifyMessage(const QString& msg);
  void addConversationTimeLine(int id);
  
private slots:
  void tabSelected(int index);
  void closeTab(int);
  void reloadTimeline(bool manual=false);
  void redrawTimeline();
  void manualReloadTimeline();
  void loadOlder();
  void pauseTimeline();
  void onVerifyCredentials(User* user);
  void onPingReady();

  void onFriendsReady(UserList);
  void onGroupsReady(QStringList);

  void exit();
  void about();
  void openbugtracker();
  void preferences();
  void newNotice(const QString& msg="", message_id_t id=-1);
  void newDM(user_id_t user_id=-1);

  void writeSettings();
  void readSettings();

  void errorMessage(const QString& msg);

  void trayIconActivated(QSystemTrayIcon::ActivationReason reason);
  void updateQtStyle();
  void updateWindowTitle();

private:
  void createActions();
  void createMenu();

  void createTrayIcon();
  void updateTrayIcon();

  void resetTimer();

  void refreshLists();

  bool sendNotification(QString summary, QString text);

  void resetNotifications();

  YaicsSettings* settings;
  YaicsSettingsDialog* settingsDialog;

  QAction* reloadAct;
  QAction* loadOlderAct;
  QAction* markAsReadAct;
  QAction* markVisibleReadAct;
  QAction* exitAct;
  QAction* prefsAct;
  QAction* aboutAct;
  QAction* aboutQtAct;
  QAction* newNoticeAct;
  QAction* newDMAct;
  QAction* pauseAct;
  QAction* bugtrackerAct;

  QMenu* fileMenu;
  QMenu* helpMenu;

  QSystemTrayIcon* trayIcon;
  QMenu* trayIconMenu;

  int timerId;

  TimeLine* homeTimeLine;
  TimeLine* mentionsTimeLine;
  TimeLine* userTimeLine;
  TimeLine* inboxTimeLine;
  TimeLine* outboxTimeLine;
  TimeLine* publicTimeLine;// = NULL;


  YaicsTabWidget* tabWidget;

  bool useTray;
  QString defaultTheme;

  static QSet<QString> groups;
  static QSet<QString> friends;

  QTemporaryFile* tempFile;
  message_id_t tempId;
  
  QSocialAPI* socialAPI;

#ifdef USE_DBUS
  QDBusInterface* dbusNotificationInterface;
#endif

  bool pingReady;
};

#endif
