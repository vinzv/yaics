/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef YAICSTABWIDGET_H
#define YAICSTABWIDGET_H

#include <QWidget>
#include <QTabBar>
#include <QTabWidget>
#include <QSignalMapper>
#include <QHash>

class YaicsTabWidget : public QTabWidget {
  Q_OBJECT

public:
  YaicsTabWidget(QWidget* parent=0);

  int addTab(QWidget* page, const QString& label, const QString& marker="",
             bool highlight=true);

   int addTab(QWidget* page, const QIcon& icon, const QString& label,
             const QString& marker="", bool highlight=true);

   QHash<QString, int> tabMarker;

public slots:
  void highlightTab(int index=-1);

  void deHighlightTab(int index=-1);

protected:
  void addHighlightConnection(QWidget* page, int index);

private:
  QSignalMapper* sMap;
};

#endif
