/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MESSAGE_EDIT_H
#define MESSAGE_EDIT_H

#include <QTextEdit>

#include "qsocialapi/status.h"

#include "yaicshighlighter.h"

//------------------------------------------------------------------------------

class MessageEdit : public QTextEdit {
  Q_OBJECT
public:
  MessageEdit(QWidget* parent=0);

signals:
  void ready();

protected:
  virtual void keyPressEvent(QKeyEvent* event);

  void complete();

private:
  YaicsHighlighter* highlighter;
};

#endif
