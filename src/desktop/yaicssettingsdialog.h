/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef YAICSSETTINGSDIALOG_H
#define YAICSSETTINGSDIALOG_H

#include <QtGui>

#include "ui_prefs.h"

#include "util/yaicssettings.h"

class YaicsSettingsDialog : public QDialog, private Ui::Prefs {
  Q_OBJECT

public:
  YaicsSettingsDialog(YaicsSettings* settings, QWidget* parent=0);

signals:
  void settingsChanged();

private slots:
  void on_buttonBox_accepted();

  void on_setLinkColourButton_clicked();

  void on_buttonBox_rejected();

protected:
  void setVisible(bool visible);

private:
  void updateUI();
  YaicsSettings* s;
};

#endif
