/*
  Copyright 2010-2015 Mats Sjöberg
  Copyright 2014-2015 Stig Atle Steffensen
  
  This file is part of the Yaics application.

  Yaics is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Yaics is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Yaics.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include <QStyleFactory>
#include "yaicsapp.h"
#include "util/yaics_defines.h"

//------------------------------------------------------------------------------

int main(int argc, char** argv) {
  QApplication app(argc, argv);

  app.setApplicationName(CLIENT_FANCY_NAME);
  app.setApplicationVersion(CLIENT_VERSION);

#ifdef IS_QT4
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
  QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));;

  if ( qgetenv("DESKTOP_SESSION") == "mate" )
    app.setStyle(QStyleFactory::create("GTK+"));
#endif

  QString locale = QLocale::system().name();
  bool startMinimized = false;

  for (int i = 1; i < app.arguments().count(); i++) {
    if (app.arguments()[i] == "-m")
      startMinimized = true;
    else if (app.arguments()[i] == "-l" && app.arguments().count() > i+1)
      locale = app.arguments()[i+1];
    else if (app.arguments()[i-1] != "-l") {
      qDebug() << "Usage: ./yaics [-m] [-l locale]";
      return 0;
    }
  }

  qDebug() << "Using locale" << locale;

  QTranslator qtTranslator;
  qtTranslator.load("qt_" + locale,
          QLibraryInfo::location(QLibraryInfo::TranslationsPath));
  app.installTranslator(&qtTranslator);

  QTranslator translator;
  bool ok = translator.load(QString("yaics_%1").arg(locale),
          ":/translations");
  app.installTranslator(&translator);

  if (ok) 
    qDebug() << "Successfully loaded translation";

  YaicsApp yapp;

  if (!startMinimized)
    yapp.show();

  return app.exec();
}
